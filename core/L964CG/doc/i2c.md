@[TOC]

# i2c
i2c操作接口
## i2c.setup( id, speed [,slaveaddr] [,isbaud])

打开i2c接口

**参数**

|参数|类型|释义|取值|
|-|-|-|-|
|id|number|i2c接口id|1,2,3,4|
|speed|number|时钟频率|0~3500000|
|[,slaveaddr]|number|可选，i2c 外设地址|0x00-0x7f|
|[,isbaud]|number|可选自定义波特率开关 0/1-关闭/开启 |0/1|

**返回值**

|返回值|类型|释义|取值|
|-|-|-|-|
|result|number|可以根据返回的频率值判断是否成功打开i2c|0~3500000|

**例子**

```lua
--使用i2c.send和i2c.recv的setup
if i2c.setup(i2cid,100000) ~= 100000 then
  print("init fail")
  return
end
--自定义i2c波特率(不想指定地址可以将第三个参数置为-1)，波特率自定义开关 第四个参数：1 打开 0 关闭
if i2c.setup(i2cid,2400,-1,1) ~= 2400 then
  print("init fail")
  return
end

--使用i2c.write和i2c.read的setup
if i2c.setup(i2cid,100000,i2cslaveaddr) ~= 100000 then
  print("init1 fail")
  return
end

```

---
# 其他接口见:
https://doc.openluat.com/wiki/21?wiki_page_id=2254



