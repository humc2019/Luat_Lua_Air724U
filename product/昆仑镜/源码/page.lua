module(..., package.seeall)

-- 字体
font = {
    -- 数显字体 (12:34)
    time = {
        path = "/lua/font_time.bin"
    },
    -- 标题的字体
    title = {
        path = "/lua/font_title.bin"
    },
    -- 其他默认字体
    def = {
        path = "/lua/font_def.bin"
    },
    -- 小数字
    gps = {
        path = "/lua/font_gps.bin"
    },
    -- 占卜
    orc = {
        path = "/lua/font_orc.bin"
    },
}

-- 图片
src = {
    -- 菜单
    menu = {
        "/lua/menu1.jpg",
        "/lua/menu2.jpg",
        "/lua/menu3.jpg",
        "/lua/menu4.jpg",
        "/lua/menu5.jpg",
        "/lua/menu6.jpg",
        "/lua/menu7.jpg",
        "/lua/menu8.jpg",
        "/lua/menu5.jpg",
    },
    -- 鸟
    bird = {
        "/lua/bird0.bin",
        "/lua/bird1.bin",
        "/lua/bird2.bin",
    },
    
    locGpsBg    = "/lua/locGpsBg.jpg"   , -- 定位星图背景
    locGpsObj   = "/lua/locGpsObj.png"  , -- 扫描的那个圈
    bgInner     = "/lua/bgInner.jpg"    , -- 子页面背景
    compassBg   = "/lua/compassBg.jpg"  , -- 罗盘背景
    compassObj  = "/lua/compassObj.jpg" , -- 罗盘的指针
    logo        = "/lua/logo.png"       , -- 合宙 logo
    birdPass    = "/lua/birdPass.png"   , -- 游戏 小鸟停止
    birdStop    = "/lua/birdStop.png"   , -- 游戏 小鸟通过
    gameover    = "/lua/gameover.png"   , -- gameover
    cloud       = "/lua/cloud.png"      , -- cloud
    msg         = "/lua/msg.jpg"        , -- 信息面板背景
    balanceBall = "/lua/balanceBall.jpg", -- 平衡的移动物体
    oracle = "/lua/oracle.jpg", 
    shake  = "/lua/shake.jpg" , 
    map0   = "/lua/map0.jpg"  , 
    map1   = "/lua/map1.jpg"  , 
    wx     = "/lua/wx.jpg"    , 
    klj    = "/lua/klj.jpg"   , 
    bagua  = "/lua/bagua.jpg" , -- 待机
}

-- 菜单界面
menu = {
    type = "cont",
    bg_opa = 0,
    child = {
        {
            -- 背景图
            type      = "img", 
            align     = lvgl.ALIGN_CENTER, 
            src       = "$menuSrc", -- 图片路径
            image_opa = "$menuOpa", -- 图片透明度
        },
        {
            -- 主标题
            type  = "label", 
            font  = font.title, 
            align = lvgl.ALIGN_CENTER, 
            text  = "$menuTxt",
        }, 
    }
}

-- 卫星标签生成函数
local function locLab(gp, gb)
    local t = {}
    for i=1, gp do
        t[#t+1] = {
            -- 条目一
            type       = "label", 
            x          = "$locGPX"..i, 
            y          = "$locGPY"..i, 
            font       = font.gps, 
            text       = "$locGPTxt"..i, 
            text_opa   = "$locGPOpa"..i, 
            text_color = lvgl.color_hex(0x15CF00) 
        }
    end
    for i=1, gb do
        t[#t+1] = {
            -- 条目一
            type       = "label", 
            x          = "$locGBX"..i, 
            y          = "$locGBY"..i, 
            font       = font.gps, 
            text       = "$locGBTxt"..i, 
            text_opa   = "$locGBOpa"..i, 
            text_color = lvgl.color_hex(0xDAF291)
        }
    end
    return t
end

-- 十颗北斗, 二十颗GPS
gbN = 10
gpN = 20

-- 星图界面
locGps = {
    type = "cont",
    bg_opa = 0,
    bg_color = lvgl.color_hex(0),
    child = {
        {
            -- 背景图
            type  = "img", 
            align = lvgl.ALIGN_CENTER, 
            src   = src.locGpsBg,
        }, 
        {
            -- 背景图
            type      = "img", 
            align     = lvgl.ALIGN_CENTER, 
            src       = src.locGpsObj, 
            angle     = "$locAngle", 
            image_opa = 80,
        }, 
        {
            type   = "cont", 
            width  = 240, 
            height = 240, 
            fit    = lvgl.FIT_NONE, 
            child  = locLab(gpN, gbN),-- 30 个左右的 table, 所以依靠函数生成了 
        }
    }
}

locMap = {
    type = "cont",
    width = 240,
    height = 240,
    fit = lvgl.FIT_NONE,
    child = {
        {
            type         = "img", 
            align        = lvgl.ALIGN_CENTER, 
            src          = "$mapSrc", 
            auto_realign = true, 
            image_opa    = "$mapOpa",
        },
        {
            type         = "label", 
            align        = lvgl.ALIGN_CENTER, 
            auto_realign = true, 
            text         = "$mapTxt",
        },
    } 
}

sigN = 5
sigW = 24
sigS = 5
sigWidth = sigN*(sigW+sigS)-sigS
function sigItem(N, lab)
    local t = {}
    for i=1, N do
        t[i] = {
            type = "cont",
            fit = lvgl.FIT_TIGHT,
            layout = lvgl.LAYOUT_CENTER,
            pad_inner = 2,
            child = {
                {
                    type       = "label", 
                    text       = "$"..lab.."sig"..i, 
                    -- font       = font.gps,
                    text_color = lvgl.color_hex(0xFFC90E),
                }, 
                {
                    type         = "cont", 
                    width        = sigW, 
                    height       = "$"..lab.."val"..i, 
                    bg_opa       = "$"..lab.."opa"..i, 
                    border_color = lvgl.color_hex(0x18B41A), 
                    border_width = 1, 
                    border_opa   = 255,
                },
                {
                    type       = "label", 
                    text       = "$"..lab.."txt"..i, 
                    text_color = lvgl.color_hex(0x18B41A),
                }, 
            }
        }
    end
    return t
end

locSig = {
    type = "cont",
    layout = lvgl.LAYOUT_CENTER,
    --fit = lvgl.FIT_MAX,
    pad_inner = 10,
    bg_color = lvgl.color_hex(0),
    bg_opa = 255,
    child = {
        {
            type       = "label", 
            text       = "GPS 信噪比", 
            text_color = lvgl.color_hex(0x18B41A),
        }, 
        {
            type      = "cont", 
            layout    = lvgl.LAYOUT_ROW_BOTTOM, 
            pad_inner = sigS, 
            height    = 70, 
            width     = sigWidth, 
            child     = sigItem(sigN, "sigGP"),
        },
        {
            type       = "label", 
            text       = "北斗 信噪比", 
            text_color = lvgl.color_hex(0x18B41A),
        }, 
        {
            type      = "cont", 
            layout    = lvgl.LAYOUT_ROW_BOTTOM, 
            pad_inner = sigS, 
            height    = 70, 
            width     = sigWidth, 
            child     = sigItem(sigN, "sigGB"),
        },
    }
}


locN = 6
function locItem(n)
    local t = {}
    for i=1, n do
        t[i] = {
            type         = "label", 
            text         = "$loc"..i, 
            auto_realign = true, 
            text_color   = lvgl.color_hex(0x18B41A),
        }
    end
    return t
end

-- GPS
locMsg = {
    type = "cont",
    bg_color = lvgl.color_hex(0),
    bg_opa = 255,
    child = {
        {
            type      = "cont", 
            layout    = lvgl.LAYOUT_CENTER, 
            fit       = lvgl.FIT_TIGHT, 
            pad_inner = 10, 
            align     = lvgl.ALIGN_CENTER, 
            child     = locItem(locN)
        }
    }
}


-- 日历界面
cld = {
    type = "cont",
    child = {
        {
            type      = "img", 
            align     = lvgl.ALIGN_CENTER, 
            src       = src.bgInner, 
            image_opa = 50
        },
        {
            type      = "cont", 
            layout    = lvgl.LAYOUT_CENTER, 
            fit       = lvgl.FIT_TIGHT, 
            pad_inner = 20, 
            align     = lvgl.ALIGN_CENTER,
            child = {
                {
                    -- 时间
                    type         = "label", 
                    font         = font.time, 
                    text         = "$cldTime", 
                    auto_realign = true,
                },
                {
                    -- 日期
                    type         = "label", 
                    font         = font.def, 
                    text         = "$cldDate", 
                    auto_realign = true,
                },
                {
                    -- 农历
                    type         = "label", 
                    font         = font.def, 
                    text         = "$cldNL", 
                    auto_realign = true,
                }
            }
        }
    }
}

-- 罗盘界面
bagua = {
    type = "cont",
    child = {
        {
            -- 指针
            type      = "img", 
            align     = lvgl.ALIGN_CENTER, 
            src       = src.bagua, 
            angle     = "$baguaAngle", 
            image_opa = 120,
        }
    } 
}

-- 罗盘界面
compass = {
    type = "cont",
    child = {
        {
            type         = "label", 
            font         = font.def, 
            text         = "$compassTxt", 
            auto_realign = true, 
            align        = lvgl.ALIGN_CENTER,
        },
        {
            type      = "img", 
            align     = lvgl.ALIGN_CENTER, 
            src       = src.compassBg, 
            image_opa = "$compassOpa2",
        },
        {
            -- 指针
            type      = "img", 
            align     = lvgl.ALIGN_CENTER, 
            src       = src.compassObj, 
            angle     = "$compassAngle", 
            image_opa = "$compassOpa1",
        },
    } 
}

orcN = 6
function orcItem(n)
    local t = {}
    for i=1, n do
        t[i] = {
            type = "label", 
            font = font.orc, 
            text = "$orc"..i,
            auto_realign = true,
        }
    end
    return t
end

-- 占卜界面
oracle = {
    type = "cont",
    bg_opa = 255,
    child = {
        {
            type = "img",
            align = lvgl.ALIGN_CENTER,
            auto_realign = true,
            src = "$oracleBg",
            angle = "$oracleAngle",
            image_opa = "$oracleOpa",
        },
        {
            type = "cont",
            layout = lvgl.LAYOUT_CENTER,
            fit = lvgl.FIT_TIGHT,
            pad_inner = 10,
            align = lvgl.ALIGN_CENTER,
            child = orcItem(orcN)
        }
    }
}

-- 平衡界面
balance = {
    type = "cont",
    bg_opa = 255,
    width = 240,
    height = 240,
    fit = lvgl.FIT_NONE,
    child = {
        {
            type = "img",
            src = src.balanceBall,
            x = "$balanceBallX",
            y = "$balanceBallY",
        },
    }
}

-- 彩灯界面
wsled = {
    type = "cont",
    bg_color = lvgl.color_hex(0),
    bg_opa = 255,
    child = {
        {
            type = "label",
            font = font.def,
            align = lvgl.ALIGN_CENTER,
            auto_realign = true,
            text_color = lvgl.color_hex(0xFFFFFF),
            text = "$ledMode",
        },
    }
}

-- 游戏界面
bird = {
    type = "cont",
    bg_opa = 255,
    width = 240,
    height = 240,
    fit = lvgl.FIT_NONE,
    bg_color = lvgl.color_hex(0xF0F0),
    child = {
        {
            -- 停止
            type = "img",
            src = src.birdStop,
            x = "$birdStopX",
            y = 0,
        },
        {
            -- 云
            type = "img",
            src = src.cloud,
            align = lvgl.ALIGN_CENTER,
            x = "$cloudX",
            y = "$cloudY",
        },
        {
            -- 通过
            type = "img",
            src = src.birdPass,
            x = "$birdPassX",
            y = "$birdPassY",
        },
        {
            -- 小鸟
            type = "img",
            src = "$birdSrc",
            x = "$birdX",
            y = "$birdY",
        },
        {
            -- 游戏结束
            type = "img",
            src = src.gameover,
            align = lvgl.ALIGN_CENTER,
            image_opa = "$gameover",
        },
        {
            type = "label",
            auto_realign = true,
            align = {lvgl.ALIGN_IN_TOP_MID, 0, 10}, 
            text = "$birdScore",
        },
    }
}

klj = {
    type = "cont",
    bg_opa = 255,
    child = {
        {
            type = "img",
            src = src.klj,
        }, 
    }
}

-- 关于界面
about = {
    type = "cont",
    bg_opa = 255,
    child = {
        {
            type = "img",
            align = {lvgl.ALIGN_CENTER, 0, -35},
            src = src.logo,
        }, 
        {
            type = "img",
            align = lvgl.ALIGN_CENTER,
            src = src.bgInner,
            image_opa = 50
        },
        {
            type = "cont",
            layout = lvgl.LAYOUT_CENTER,
            fit = lvgl.FIT_TIGHT,
            pad_inner = 10,
            align = {lvgl.ALIGN_CENTER, 0, 65},
            child = {
                {
                    type = "label",
                    font = font.def,
                    text = "上海合宙通信科技"
                }, 
                {
                    type = "label", 
                    font = font.def, 
                    text = "有限公司"
                }
            }
        }
    }
}

-- 关机界面
msg = {
    type = "cont",
    fit = lvgl.FIT_MAX,
    align = lvgl.ALIGN_CENTER,
    child = { 
        {
            type = "img",
            align = lvgl.ALIGN_CENTER,
            src = src.msg,
            image_opa = 50,
        },
        {
            type = "label",
            font = font.def,
            align = {lvgl.ALIGN_CENTER, 0, -20},
            text = "是否确认关机?",
        },
        {
            type = "cont",
            layout = lvgl.LAYOUT_PRETTY_MID,
            width = 120,
            pad_inner = 30,
            align = {lvgl.ALIGN_CENTER, 0, 40},
            child = {
                {
                    type = "label",
                    font = font.def,
                    text = "关机"
                },
                {
                    type = "label",
                    font = font.def,
                    text = "取消"
                },
            }
        }
    }
}
