-- 基础页面
module(..., package.seeall)

-- 默认按键处理
-- 其他界面中未定义的按键都会调用这里的处理方法
function keyExitShort() 
    wsled.unlight()
    airui.show("menu")
end

function keyPowerShort() 
    airui.show("msg")
end

function keyPowerLong() 
    airui.show("msg")
end
