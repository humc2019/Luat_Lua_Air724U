-- 定位里的 信息 页面
module(..., package.seeall)

-- RTK 定位方式
function printGps()

    if tools.testMode then
        page.loc1 = "定位状态:已经定位"
        page.loc2 = "定位方式:普通"
        page.loc3 = "高度:18"
        page.loc4 = "速度:0 km/h"
        page.loc5 = "经度:121.6009976"
        page.loc6 = "纬度:31.1755963"
        return 
    end

    if not (gps and gps.isOpen()) then
        page.loc1 = " "
        page.loc2 = " "
        page.loc3 = "GPS 未打开"
        page.loc4 = " "
        page.loc5 = " "
        page.loc6 = " "
        return
    end
    if not gps.isFix() then
        page.loc1 = "定位状态:未定位"
        page.loc2 = "定位方式:普通"
        page.loc3 = "高度:0000"
        page.loc4 = "速度:0 km/h"
        page.loc5 = "经度:000.000000"
        page.loc6 = "纬度:00.0000000"
        return
    end
    local t = gps.getLocation()
    page.loc1 = "定位状态:已定位"
    page.loc2 = "定位方式:普通"
    page.loc3 = "高度:"..(gps.getAltitude() or 0)
    page.loc4 = "速度:"..(gps.getSpeed() or 0).." km/h"
    page.loc5 = "RTK经度:"..(t.lng or 0)
    page.loc6 = "RTK纬度:"..(t.lat or 0)
end

function enter()
    printGps()
    sys.timerLoopStart(printGps, 2000)
end

function exit()
    sys.timerStop(printGps)
end


-- 上一页
function keyUpShort() 
    airui.show("locSig")
end

-- 下一页
function keyDownShort() 
    airui.show("locRtk", "locMsg")
end
