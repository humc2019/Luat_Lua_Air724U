-- 菜单页面
module(..., package.seeall)

-- 主菜单
menuNow = 1

waitTime = 20000
function showW()
    airui.show("bagua")
end

function showWait()
    -- menuStatus[menuNow].id, menuStatus[menuNow].page
    sys.timerStart(showW, waitTime)
end

local img = page.src.menu
local imgN = 0
function srcIdx()
    imgN = imgN % #img + 1
    return img[imgN]
end

menuStatus = {
    {menuSrc = srcIdx(), menuTxt = "定位", id = "locGps"},
    {menuSrc = srcIdx(), menuTxt = "地图", id = "locMap"}, 
    {menuSrc = srcIdx(), menuTxt = "占卜", id = "oracle"},
    {menuSrc = srcIdx(), menuTxt = "平衡", id = "balance"}, 
    {menuSrc = srcIdx(), menuTxt = "日期", id = "cld"}, 
    {menuSrc = srcIdx(), menuTxt = "罗盘", id = "compass"}, 
    {menuSrc = srcIdx(), menuTxt = "彩灯", id = "wsled"},
    {menuSrc = srcIdx(), menuTxt = "游戏", id = "bird"},
    {menuSrc = srcIdx(), menuTxt = "关于", id = "about"},
}

-- 菜单
page.menuOpa = 50                          -- 背景透明度
page.menuSrc = menuStatus[menuNow].menuSrc -- 菜单背景
page.menuTxt = menuStatus[menuNow].menuTxt -- 标题文字

-- 淡入淡出效果
-- 透明数值记录, 切换动作被打断需还原透明度
local bak
-- 切换锁
local lock = false
function inout(v)
    if lock then return end
    local step = 6
    lock = true
    sys.taskInit(function()
        bak = page.menuOpa or 50
        -- 淡出
        while page.menuOpa>step do
            page.menuOpa = page.menuOpa - step
            sys.wait(100)
            if not lock then return end
        end
        -- 换图
        page.menuSrc = v
        -- 淡入
        while page.menuOpa<bak do
            -- 淡入速度需要稍快一点
            page.menuOpa = page.menuOpa + step*1.5
            sys.wait(100)
            if not lock then return end
        end
        page.menuOpa = bak
        lock = false
    end)
end

-- 上一页
function keyUpShort()                                                                                                                                                                                                                                                                                         
    sys.timerStart(showW, waitTime)
    menuNow = menuNow - 1
    if menuNow <= 0 then menuNow = #menuStatus end
    page.menuTxt = menuStatus[menuNow].menuTxt
    inout(menuStatus[menuNow].menuSrc)
end

-- 下一页
function keyDownShort() 
    sys.timerStart(showW, waitTime)
    menuNow = menuNow + 1
    if menuNow > #menuStatus then menuNow = 1 end
    page.menuTxt = menuStatus[menuNow].menuTxt  
    inout(menuStatus[menuNow].menuSrc)
end

-- 退出后停止淡入淡出
function exit()
    sys.timerStop(showW)
    lock = false
    if bak then
        page.menuOpa = bak
    end
end

function enter()
    sys.timerStart(showW, waitTime)
end

-- 进入
function keyOkShort()    
    airui.show(menuStatus[menuNow].id, menuStatus[menuNow].page)
end

function keyExitShort() 
    -- 菜单界面屏蔽退出按键
end
