-- 待机页面
module(..., package.seeall)



-- 动画
function animation()
    page.baguaAngle = page.baguaAngle + 100
    if page.baguaAngle >= 3600 then
        page.baguaAngle = 0
    end
end

-- 上一页
function keyUpShort()
    airui.show("menu")
end

-- 下一页
function keyDownShort()
    airui.show("menu")
end

-- 退出
function enter()
    page.baguaAngle = 0
    sys.timerLoopStart(animation, 100)
end

-- 进入
function exit()
    sys.timerStop(animation)
end
