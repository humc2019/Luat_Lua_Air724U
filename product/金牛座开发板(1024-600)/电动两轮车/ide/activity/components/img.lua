module(..., package.seeall)

function create_style(v)
    if not v then v = {} end

    local s = lvgl.style_t()

    lvgl.style_init(s)

    -- image_blend_mode ( lvgl.blend_mode_t )：设置图像的混合模式。
    -- 可以是 lvgl.BLEND_MODE_NORMAL/ADDITIVE/SUBTRACTIVE。
    -- 默认值：lvgl.BLEND_MODE_NORMAL。
    -- lvgl.style_set_image_blend_mode(s, lvgl.STATE_DEFAULT, lvgl.BLEND_MODE_NORMAL)
    if v.opa then
        lvgl.style_set_image_opa(s, lvgl.STATE_DEFAULT, v.opa or 255)
    end
    if v.recolor then
        lvgl.style_set_image_recolor_opa(s, lvgl.STATE_DEFAULT,
                                         v.recolor_opa or 255)
        lvgl.style_set_image_recolor(s, lvgl.STATE_DEFAULT,
                                     lvgl.color_hex(v.recolor or 0xFF0000))
    end
    return s
end

function create(p, v)
    local img = lvgl.img_create(p, nil)
    lvgl.obj_set_click(img, v.click or false)
    -- lvgl.obj_set_size(contentBodyBG, LCD_W, LCD_H * 7 / 8)
    -- 缩放不可用
    -- lvgl.img_set_zoom(img, v.zoom or lvgl.IMG_ZOOM_NONE)
    lvgl.img_set_src(img, v.src)

    lvgl.obj_align(img, v.align_to, v.align or lvgl.ALIGN_IN_TOP_MID,
                   v.align_x or 0, v.align_y or 0)
    lvgl.img_set_pivot(img, v.pivot_x or 0, v.pivot_y or 0)
    lvgl.img_set_angle(img, v.angle or 0)

    lvgl.obj_add_style(img, lvgl.IMG_PART_MAIN, create_style(v.style))

    return img
end