module(..., package.seeall)

font12 = lvgl.font_load(spi.SPI_1, 12, 2, 10)

font14 = lvgl.font_load(spi.SPI_1, 14, 2, 15)

font16 = lvgl.font_load(spi.SPI_1, 16, 2, 20)

font18 = lvgl.font_load(spi.SPI_1, 18, 2, 40)

font24 = lvgl.font_load(spi.SPI_1, 24, 2, 45)

font36 = lvgl.font_load(spi.SPI_1, 36, 2, 60)

font48 = lvgl.font_load(spi.SPI_1, 48, 2, 80)

Bfont48 = lvgl.font_load(spi.SPI_1, 48, 4, 200)

font54 = lvgl.font_load(spi.SPI_1, 54, 2, 190)

font68 = lvgl.font_load(spi.SPI_1, 68, 2, 190)

font96 = lvgl.font_load(spi.SPI_1, 96, 1, 190)
