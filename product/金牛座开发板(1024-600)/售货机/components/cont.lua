module(..., package.seeall)


function create(p, v)
    c = lvgl.cont_create(p, nil)
    lvgl.cont_set_fit(c, lvgl.FIT_NONE)
    lvgl.obj_set_click(c, v.click or true)
    lvgl.obj_set_size(c, v.W or 100, v.H or 100)
    -- LAYOUT_CENTER
    -- LAYOUT_COLUMN_LEFT
    -- LAYOUT_COLUMN_MID
    -- LAYOUT_COLUMN_RIGHT
    -- LAYOUT_GRID
    -- LAYOUT_OFF
    -- LAYOUT_PRETTY_BOTTOM
    -- LAYOUT_PRETTY_MID
    -- LAYOUT_PRETTY_TOP
    -- LAYOUT_ROW_BOTTOM
    -- LAYOUT_ROW_MID
    -- LAYOUT_ROW_TOP

    if v.layout then lvgl.cont_set_layout(c, v.layout) end
    -- lvgl.obj_set_auto_realign(Titlecont, true)                   
    lvgl.obj_align(c, v.align_to, v.align or lvgl.ALIGN_CENTER, v.align_x or 0,
                   v.align_y or 0)

    if v.event then lvgl.obj_set_event_cb(c, v.event) end
    lvgl.obj_add_style(c, lvgl.CONT_PART_MAIN, common_style.create_style(v.style))
    return c
end

-- create(lvgl.scr_act(), {
--     W = 200,
--     H = 100,
--     click = false,
--     style = {
--         border = {color = 0x0f0f0f, width = 2, opa = 30},
--         shadow = {spread = 30, color = 0xff00f0},
--         bg = {
--             radius = 10,
--             color = 0x0f0ff0,
--             opa = 150,
--             grad = {color = 0x0f0f0f}
--         }
--     }
-- })
