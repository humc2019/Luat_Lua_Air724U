module(..., package.seeall)

function create(p, v)

    local event_handler = function(obj, event)
        if event == lvgl.EVENT_CLICKED then print("按键默认回调") end
    end

    local obj = lvgl.btn_create(p, nil)
    lvgl.obj_set_size(obj, v.W or 100, v.H or 220)
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    -- 可以使用 lvgl.btn_set_checkable(btn, true) 将按钮配置为切换按钮。在这种情况下，
    -- 单击时，按钮将自动进入lvgl.STATE_CHECKED 状态，或再次单击时返回到lvgl.STATE_CHECKED状态。
    -- lvgl.btn_set_checkable(obj, true)

    -- lvgl.btn_set_layout(btn,lvgl.LAYOUT_...) 设置布局。默认值为lvgl.LAYOUT_CENTER 。
    -- 因此，如果添加标签，则标签将自动与中间对齐，并且无法通过 lvgl.obj_set_pos() 移动。
    -- 您可以使用 lvgl.btn_set_layout(btn,lvgl.LAYOUT_OFF) 禁用布局。
    lvgl.btn_set_layout(obj, lvgl.LAYOUT_CENTER)

    -- lvgl.btn_set_state(obj, lvgl.BTN_STATE_CHECKED_RELEASED)
    -- lvgl.btn_toggle(b)
    -- lvgl.btn_set_fit2(obj, lvgl.FIT_NONE, lvgl.FIT_TIGHT)

    lvgl.obj_set_event_cb(obj, v.event or event_handler)

    if v.label then label.create(obj, v.label) end

    -- lvgl.obj_set_style_local_value_str(obj, lvgl.BTN_PART_MAIN,
    --                                    lvgl.STATE_DEFAULT, "Ripple")

    lvgl.obj_add_style(obj, lvgl.BTN_PART_MAIN,
                       common_style.create_style(v.style))

    return obj
end
aaa = "测试"
local data = {
    W = 300,
    H = 200,
    -- label = {
    --     W = 300,
    --     H = 200,
    --     style = {
    --         border = {color = 0x00ff0f, width = 2, opa = 200},
    --         shadow = {spread = 30, color = 0xff00f0},
    --         bg = {
    --             radius = 10,
    --             color = 0x0f0ff0,
    --             opa = 150,
    --             grad = {color = 0x0f0f0f}
    --         },
    --         text = {
    --             font = style.font68,
    --             color = 0xff0000,
    --             letter_space = 20,
    --             line_space = 20
    --         }
    --     },
    --     text = "#ff0000 hh# #0f0f00 hhh# #f0ff00 hhh# #0f0ff0 h "
    -- },
    style = {
        border = {color = 0x0f0f0f, width = 2, opa = 200},
        shadow = {spread = 10, color = 0xff00f0},
        value = {
            -- font = style.font24,
            str = aaa,
            color = 0xff0000,
            opa = 200
        },
        bg = {
            radius = 10,
            color = 0x0f0ff0,
            opa = 150,
            grad = {color = 0x0f0f0f}
        }
    }
}

-- create(lvgl.scr_act(), data)
