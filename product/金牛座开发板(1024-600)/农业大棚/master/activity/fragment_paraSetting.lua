module(..., package.seeall)

local topicTable={}
local function topicTableAdd(id,callback)
    topicTable[id]=callback
    sys.subscribe(id,callback)
end

function initParaSetting(runningBox,t)

    local modeLabelText,paraCont,paraLabelUP,paraLabelDown
    local Status_mode,Status_paraUP,Status_paraDown
    local temp_mode,temp_paraUP,temp_paraDown

    local function paraSave()

        for id, callback in pairs(topicTable) do
            sys.unsubscribe(id, callback)
        end
        topicTable={}

        temp_paraUP=tonumber(lvgl.textarea_get_text(paraLabelUP))
        temp_paraDown=tonumber(lvgl.textarea_get_text(paraLabelDown))
        if temp_mode==Status_mode and temp_paraUP==Status_paraUP and temp_paraDown==Status_paraDown then return end
        t.run.ctl=temp_mode
        if temp_mode==0 then
            t.set.ontime=temp_paraUP
            t.set.offtime=temp_paraDown
        elseif temp_mode==1 then
            t.set.humih=temp_paraUP
            t.set.humil=temp_paraDown
        end 
        sys.publish("PARASETTING_CHANGE",t.path)

        -- print(paraLabelUP)
    end



    local labelBox=lvgl.cont_create(runningBox, nil)
    lvgl.obj_set_click(labelBox,false)
    lvgl.obj_set_size(labelBox, 560,60) 
    lvgl.obj_add_style(labelBox, lvgl.CONT_PART_MAIN, style.style_divBox)

    local label=lvgl.label_create(labelBox, nil)
    lvgl.label_set_recolor(label, true) 
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(label, "#E67517 参数设置")
    lvgl.obj_align(label, labelBox, lvgl.ALIGN_IN_LEFT_MID,20,0)


    function saveHandler(obj, event)
        if event == lvgl.EVENT_CLICKED then
            _G.BEEP()
            -- print("保存按钮按下")
            paraSave()
            fragment_RunStatus.init(runningBox)
        end
    end

    local save=lvgl.img_create(labelBox, nil)
    lvgl.obj_set_click(save,true)
    lvgl.img_set_src(save,"/lua/save_icon.png")
    lvgl.obj_align(save,labelBox, lvgl.ALIGN_IN_RIGHT_MID, -20, 0)
    lvgl.obj_set_event_cb(save,saveHandler)
    
    local line=lvgl.cont_create(labelBox, nil)
    lvgl.obj_set_click(line,false)
    lvgl.obj_set_size(line, 560, 1) 
    lvgl.obj_add_style(line, lvgl.CONT_PART_MAIN, style.style_line)
    lvgl.obj_align(line, labelBox, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)

    --支路选择
    local pathLabel=lvgl.label_create(runningBox, nil)
    lvgl.obj_set_style_local_text_font(pathLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(pathLabel, "支路选择:")
    lvgl.obj_align(pathLabel, labelBox, lvgl.ALIGN_OUT_BOTTOM_LEFT, 20, 30)

    local RunningSatusTable=UIConfig.getBranchTable()
    local option=""
    local id=0
    for i=1,#RunningSatusTable do
        local v=RunningSatusTable[i]
        if v.path==t.path then id=i-1 end
        option=option..v.path
        if i<#RunningSatusTable then option=option.."\n" end
    end

    local event_handler = function(obj, event)
        if event == lvgl.EVENT_CLICKED then
            _G.BEEP()
        elseif event == lvgl.EVENT_VALUE_CHANGED then
            _G.BEEP()
            paraSave()
            local index=lvgl.dropdown_get_selected(obj)
            print("Option:",index)
            initParaSetting(runningBox,RunningSatusTable[index+1])
            -- changeMode()
        end
    end

    -- lv_style_set_text_font(&style_screen_ddlist0_selected, LV_STATE_DEFAULT, &lv_font_simsun_12);

    local dd = lvgl.dropdown_create(runningBox, nil)
    lvgl.obj_add_style(dd, lvgl.DROPDOWN_PART_SELECTED, style.style_labelBgBlue_noRadius)
    lvgl.obj_add_style(dd, lvgl.DROPDOWN_PART_LIST, style.style_list_BG_font)
    lvgl.obj_add_style(dd, lvgl.DROPDOWN_PART_MAIN, style.style_list_BG)
    lvgl.obj_set_style_local_text_font(dd, lvgl.DROPDOWN_PART_SELECTED,lvgl.STATE_DEFAULT, font24)
    lvgl.obj_set_style_local_text_font(dd, lvgl.DROPDOWN_PART_LIST,lvgl.STATE_DEFAULT, font24)
    lvgl.obj_set_style_local_text_font(dd, lvgl.DROPDOWN_PART_MAIN,lvgl.STATE_DEFAULT, font24)
    lvgl.dropdown_set_options(dd, option)
    lvgl.dropdown_set_selected(dd, id)
    -- 设置对齐
    lvgl.obj_align(dd, pathLabel, lvgl.ALIGN_OUT_RIGHT_MID, 30, 0)
    lvgl.obj_set_event_cb(dd, event_handler)

    --模式控制
    local modeLabel=lvgl.label_create(runningBox, nil)
    lvgl.obj_set_style_local_text_font(modeLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(modeLabel, "模式控制:")
    lvgl.obj_align(modeLabel,pathLabel, lvgl.ALIGN_OUT_BOTTOM_LEFT, 0, 30)

    local mode_change=function(obj,event)
        if event == lvgl.EVENT_CLICKED then
            _G.BEEP()
            local text=(temp_mode == 0 and "光强" or "时间")
            lvgl.label_set_text(modeLabelText, text)
            if temp_mode==1 then
                temp_mode = 0
                paraLabelUP,paraLabelDown=initParacontByTime(paraCont)
            elseif temp_mode==0 then
                temp_mode = 1
                paraLabelUP,paraLabelDown=initParacontByRH(paraCont)
            end
        end
    end
    local modeLabelCont=lvgl.cont_create(runningBox, nil)
    -- lvgl.obj_set_click(modeLabelCont,false)
    lvgl.obj_set_event_cb(modeLabelCont, mode_change)
    lvgl.obj_set_size(modeLabelCont, 100,50)
    lvgl.obj_add_style(modeLabelCont, lvgl.CONT_PART_MAIN, style.style_labelBgBlue)
    lvgl.cont_set_layout(modeLabelCont,lvgl.LAYOUT_CENTER)
    lvgl.obj_align(modeLabelCont,modeLabel, lvgl.ALIGN_OUT_RIGHT_MID, 30, 0)

    modeLabelText=lvgl.label_create(modeLabelCont, nil)
    lvgl.label_set_recolor(modeLabelText, true) 
    lvgl.obj_set_style_local_text_font(modeLabelText, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.obj_set_style_local_text_color(modeLabelText,lvgl.LABEL_PART_MAIN,lvgl.STATE_DEFAULT,lvgl.color_hex(0xFFFFFF))
    local ctlTab={"时间","光强"}
    Status_mode=t.run.ctl
    temp_mode=Status_mode
    lvgl.label_set_text(modeLabelText,ctlTab[t.run.ctl+1])

    --参数设置
    local paraSettingLabel=lvgl.label_create(runningBox, nil)
    lvgl.obj_set_style_local_text_font(paraSettingLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(paraSettingLabel, "参数设置:")
    lvgl.obj_align(paraSettingLabel,modeLabel, lvgl.ALIGN_OUT_BOTTOM_LEFT, 0, 30)

    paraCont=lvgl.cont_create(runningBox, nil)
    lvgl.obj_set_click(paraCont,false)
    lvgl.obj_set_size(paraCont, 250,120)
    lvgl.obj_add_style(paraCont, lvgl.CONT_PART_MAIN, style.style_divBox)
    lvgl.obj_align(paraCont,paraSettingLabel, lvgl.ALIGN_OUT_RIGHT_TOP, 30,0)


    function paddiingParaCont_Item(cont,bgtextL,bgtextR,para,align,x,y)

        local paraSettingCont=lvgl.cont_create(cont, nil)
        lvgl.obj_set_click(paraSettingCont,false)
        lvgl.obj_set_size(paraSettingCont, 200,50)
        -- lvgl.obj_set_auto_realign(paraSettingCont, true)
        -- lvgl.cont_set_fit(paraSettingCont, lvgl.FIT_TIGHT)
        lvgl.obj_add_style(paraSettingCont, lvgl.CONT_PART_MAIN, style.style_labelBgSnowWhite)
        lvgl.obj_align(paraSettingCont,cont,align,x,y)


        local paraBox=inputBox.init(paraSettingCont,45,10,lvgl.ALIGN_CENTER,0,0,para)

        local BGLabelLeft=lvgl.label_create(paraSettingCont, nil)
        lvgl.label_set_recolor(BGLabelLeft, true) 
        lvgl.obj_set_style_local_text_font(BGLabelLeft, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
        lvgl.label_set_text(BGLabelLeft,bgtextL)
        lvgl.obj_align(BGLabelLeft,paraBox, lvgl.ALIGN_OUT_LEFT_MID, 0, 0)

        local BGLabelRight=lvgl.label_create(paraSettingCont, nil)
        lvgl.label_set_recolor(BGLabelRight, true) 
        lvgl.obj_set_style_local_text_font(BGLabelRight, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
        lvgl.label_set_text(BGLabelRight,bgtextR)
        lvgl.obj_align(BGLabelRight,paraBox, lvgl.ALIGN_OUT_RIGHT_MID, 0, 0)

        return paraBox
    end

    function initParacontByRH(cont)
        lvgl.obj_clean(cont)
        Status_paraUP=t.set.humih
        Status_paraDown=t.set.humil
        local labelUP=paddiingParaCont_Item(cont,"上限","LUX",Status_paraUP,lvgl.ALIGN_IN_TOP_LEFT, 0, 0)
        local labeltDown=paddiingParaCont_Item(cont,"下限","LUX",Status_paraDown,lvgl.ALIGN_IN_BOTTOM_LEFT, 0, 0)
        return labelUP,labeltDown
    end

    function initParacontByTime(cont)
        lvgl.obj_clean(cont)
        Status_paraUP=t.set.ontime
        Status_paraDown=t.set.offtime
        local labelUP=paddiingParaCont_Item(cont,"开启","秒",Status_paraUP,lvgl.ALIGN_IN_TOP_LEFT, 0, 0)
        local labeltDown=paddiingParaCont_Item(cont,"关闭","秒",Status_paraDown,lvgl.ALIGN_IN_BOTTOM_LEFT, 0, 0)
        return labelUP,labeltDown
    end


    -- function changeMode(p,text)
    --     lvgl.label_set_text(p,"#FFFFFF "..text)
    -- end

    function changePara(p,text)
        lvgl.label_set_text(p,"#FFFFFF "..text)
    end

    -- local labelUP,labeltDown

    if t.run.ctl==0 then--时间
        paraLabelUP,paraLabelDown=initParacontByTime(paraCont)
    elseif t.run.ctl==1 then--湿度
        paraLabelUP,paraLabelDown=initParacontByRH(paraCont)
    end

    -- changePara(paraLabelUP,80)
    -- changePara(paraLabelDown,100)
    -- changePara(modeLabelText,t.mode)
    -- lvgl.label_set_text(labelUP,80)
    -- lvgl.label_set_text(labeltDown,99)

    local function paraChange()
        Status_mode=t.run.ctl
        temp_mode=Status_mode
        Status_paraUP=t.set.humih
        Status_paraDown=t.set.humil
        lvgl.label_set_text(modeLabelText,ctlTab[t.run.ctl+1])
        if temp_mode==0 then
            paraLabelUP,paraLabelDown=initParacontByTime(paraCont)
        elseif temp_mode==1 then
            paraLabelUP,paraLabelDown=initParacontByRH(paraCont)
        end
    end

    topicTableAdd(t.path.."_PARACHANGE",paraChange)
end


function init(runningBox,t)
    lvgl.obj_clean(runningBox)
    initParaSetting(runningBox,t)
end

function uninit()
    -- print("取消订阅")
    for id, callback in pairs(topicTable) do
        sys.unsubscribe(id, callback)
    end
end