----------------------------------------------------------------------------
-- 1. This file automatically generates code for the LuatOS's UI designer
-- 2. In case of accident, modification is strictly prohibited
----------------------------------------------------------------------------

--Import event file
require "UiHandle"

local function objectHide()
	local o = {}
	local tSelf = {}
	setmetatable(o, tSelf)
	tSelf.__index = tSelf
	tSelf.__tostring = function(j)
		return j.self
	end
	tSelf.__tocall = function(j)
		return j.cb
	end
	tSelf.self = nil
	tSelf.cb = function(e) end
	return o
end

ScreenA = 
{
	create = nil, 
	free = nil,
	contFather_ScreenA = nil,
	LvglImg1 = objectHide(),
	LvglImg2 = objectHide(),
}
--This is default style of cont which border is invisible
lvgl_UiDesigner_DefaultContStyle = lvgl.style_t()
lvgl.style_init(lvgl_UiDesigner_DefaultContStyle)
lvgl.style_set_radius(lvgl_UiDesigner_DefaultContStyle, (lvgl.STATE_DEFAULT or LV_STATE_FOCUSED or LV_STATE_PRESSED), 0)
lvgl.style_set_border_opa(lvgl_UiDesigner_DefaultContStyle, (lvgl.STATE_DEFAULT or LV_STATE_FOCUSED or LV_STATE_PRESSED), 0)

local function lvgl_UiDesigner_DefOutCb(o, e, output)
	if e == lvgl.EVENT_CLICKED then
		lvgl.obj_set_hidden(output, false)
		lvgl.keyboard_set_textarea(output, o)
	elseif e == lvgl.EVENT_DEFOCUSED then
		lvgl.obj_set_hidden(output, true)
	elseif e == lvgl.EVENT_VALUE_CHANGED then
		sys.publish("UI_EVENT_IND", o, e)
	end
end

local function lvgl_UiDesigner_DefInCb(o, e)
	lvgl.keyboard_def_event_cb(o, e)
	if e == lvgl.EVENT_CANCEL or e == lvgl.EVENT_APPLY then
		lvgl.obj_set_hidden(o, true)
	end
end

----------------------------------------------------------------------------
--The following is the content of screen: ScreenA
---------------------------------------------------------------------------
ScreenA.create = function()
	ScreenA.contFather_ScreenA = lvgl.cont_create(lvgl.scr_act(), nil)
	lvgl.obj_set_size(ScreenA.contFather_ScreenA, 1024, 600)
	lvgl.obj_align(ScreenA.contFather_ScreenA, nil, lvgl.ALIGN_IN_TOP_LEFT, 0, 0)
	lvgl.obj_add_style(ScreenA.contFather_ScreenA, lvgl.CONT_PART_MAIN, lvgl_UiDesigner_DefaultContStyle)

	--This is the IMG_PART_MAIN's style of ScreenA.LvglImg1
	Style_LvglImg1_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglImg1_1)

	--This is the base code of ScreenA.LvglImg1
	ScreenA.LvglImg1.self = lvgl.img_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglImg1.self, true)
	lvgl.img_set_src(ScreenA.LvglImg1.self, "/lua/bg2.jpg")
	lvgl.img_set_zoom(ScreenA.LvglImg1.self, 256)
	lvgl.img_set_pivot(ScreenA.LvglImg1.self, 0, 0)
	lvgl.obj_align(ScreenA.LvglImg1.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 0, 0)
	lvgl.obj_add_style(ScreenA.LvglImg1.self, lvgl.IMG_PART_MAIN, Style_LvglImg1_1)


	--This is the IMG_PART_MAIN's style of ScreenA.LvglImg2
	Style_LvglImg2_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglImg2_1)

	--This is the base code of ScreenA.LvglImg2
	ScreenA.LvglImg2.self = lvgl.img_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglImg2.self, true)
	lvgl.img_set_src(ScreenA.LvglImg2.self, "/lua/1.jpg")
	lvgl.img_set_zoom(ScreenA.LvglImg2.self, 256)
	lvgl.img_set_pivot(ScreenA.LvglImg2.self, 0, 0)
	lvgl.obj_align(ScreenA.LvglImg2.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 274, 156)
	lvgl.obj_add_style(ScreenA.LvglImg2.self, lvgl.IMG_PART_MAIN, Style_LvglImg2_1)
	--This is to add callback function for ScreenA.LvglImg2
	--This is callBack function of ScreenA.LvglImg2
	local handleLvglImg2 = function(obj, e)
		ScreenA.LvglImg2.cb(e)
		ScreenA.LvglImg2.cb = function(e)
			if (e == lvgl.EVENT_CLICKED)then
				change()
				sys.publish("UI_EVENT_IND", obj, e)
			end
		end
	end
	lvgl.obj_set_event_cb(ScreenA.LvglImg2.self, handleLvglImg2)
end
ScreenA.free = function()
	lvgl.obj_del(ScreenA.contFather_ScreenA)
	ScreenA.contFather_ScreenA = nil
end


----------------------------------------------------------------------------
-----------------------This is the Initial of lvglGUI-----------------------
----------------------------------------------------------------------------
function lvglUiInitial()
	ScreenA.create()
end
