module(..., package.seeall)



function create(p, v)
    local obj = lvgl.qrcode_create(p, nil)
    lvgl.obj_set_click(obj, v.click or true)
    lvgl.obj_set_size(obj, v.W or 200, v.H or 200)
    lvgl.qrcode_set_txt(obj, v.text or "https://doc.openluat.com/home")

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    -- lvgl.obj_add_style(obj, lvgl.QRCODE_PART_MAIN, create_style(v.style))
    return obj
end

