module(..., package.seeall)

function create(p, v)
    local obj = lvgl.roller_create(p, nil)
    if not v then return obj end
    if not v.click then lvgl.obj_set_click(obj, false) end
    lvgl.obj_set_size(obj, v.W or 300, v.H or 200)

    -- 这些选项作为带有 lvgl.roller_set_options(obj, options, lvgl.ROLLER_MODE_NORMAL/INFINITE) 的字符串传递给Roller。
    -- 选项应用 \n 分隔。例如： "First\nSecond\nThird" 。
    -- lvgl.ROLLER_MODE_INFINITE 使滚子呈圆形。
    -- ROLLER_MODE_INFINITE
    -- ROLLER_MODE_NORMAL

    if v.options then
        lvgl.roller_set_options(obj, v.options, v.mode and
                                    lvgl.ROLLER_MODE_INFINITE or
                                    lvgl.ROLLER_MODE_NORMAL)
    end
    -- 可以使用 lvgl.roller_set_selected(obj, id, lvgl.ANIM_ON/OFF) 手动选择选项，其中id是选项的索引。
    -- 使用 lvgl.roller_get_selected(obj) 获取当前选定的选项，它将返回选定选项的索引。
    -- lvgl.roller_get_selected_str(obj, buf, buf_size) 将所选选项的名称复制到 buf 。

    -- 要水平对齐标签，请使用 lvgl.roller_set_align(obj, lvgl.LABEL_ALIGN_LEFT/CENTER/RIGHT) 水平对齐标签。
    -- 默认居中。
    lvgl.roller_set_align(obj, lvgl.LABEL_ALIGN_CENTER)

    -- 可见行数可以通过 lvgl.roller_set_visible_row_count(obj, num) 进行调整
    if v.row then lvgl.roller_set_visible_row_count(obj, v.row) end

    -- 当滚轴滚动且未完全停在某个选项上时，它将自动滚动到最近的有效选项。
    -- 可以通过 lvgl.roller_set_anim_time(obj, anim_time) 更改此滚动动画的时间。动画时间为零表示没有动画。
    if v.anim_time then lvgl.roller_set_anim_time(obj, v.anim_time) end

    -- 滚筒的主要部件称为 lvgl.ROLLER_PART_BG 。
    -- 它是一个矩形，并使用所有典型的背景属性。 Roller标签的样式继承自背景的文本样式属性。 
    -- 要调整选项之间的间距，请使用text_line_space样式属性。填充样式属性设置了侧面的空间。
    -- 中间的选定选项可以用 lvgl.ROLLER_PART_SELECTED 虚拟零件引用。除了典型的背景属性外，
    -- 它还使用文本属性来更改所选区域中文本的外观。
    if v.style then
        lvgl.obj_add_style(obj, lvgl.ROLLER_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.ROLLER_PART_SELECTED,
                           create_style(v.style.selected))
    end

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    return obj
end

local roller_data = {
    options = "FirstFirstFirst\nSecond\nThird\nThird\nThird\nThird\nThird\nThird",
    mode = false,
    anim_time = 500,
    style = {
        bg = {
            text = {font = style.font24, line_space = 25},
            pad = {left = 30, right = 30},
            bg = {radius = 10, color = 0x00ffff, opa = 255}
        },
        selected = {
            text = {font = style.font68, color = 0xff0000, line_space = 25},
            bg = {radius = 10, color = 0xffcccc, opa = 255}
        }
    }
}
-- create(lvgl.scr_act(), roller_data)
