module(..., package.seeall)
_G.home_cont = cont.create(lvgl.scr_act(), {
    W = LCD_W,
    H = LCD_H,
    click = false,
    style = {border = {width = 0}, bg = {color = 0xffcccc}}
})

_G.img_cont = cont.create(home_cont, {
    W = LCD_W,
    H = LCD_H * 0.8,
    align = lvgl.ALIGN_IN_TOP_MID,
    click = false,
    style = {border = {width = 0}, bg = {color = 0xffcccc}}
})

img_list = img.create(img_cont, {align = lvgl.ALIGN_IN_TOP_MID, src = "3.jpg"})

local img_data = {align = lvgl.ALIGN_IN_BOTTOM_MID, src = "bottom.jpg"}

bottom_img = img.create(lvgl.scr_act(), img_data)

qrcode.create(bottom_img, {
    text = "https://doc.openluat.com/home",
    W = 125,
    H = 125,
    align = lvgl.ALIGN_IN_RIGHT_MID,
    align_x = -65,
})

local count = 1

sys.timerLoopStart(function()
    count = count % 3 + 1
    local src = "/lua/" .. count .. ".jpg"
    lvgl.img_set_src(img_list, src)

end, 2000)

