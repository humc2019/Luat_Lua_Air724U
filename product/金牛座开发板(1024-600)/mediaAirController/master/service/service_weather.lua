module(..., package.seeall)
--------------
_G.weatherTable={}

local weatherImg={
    [100]="icon_sunny.png",
    [101]="icon_duoyun.png",
    [104]="icon_cloudy.png",
    [150]="icon_sunny_night.png",
    [151]="icon_duoyun_night.png",
    [153]="icon_duoyun.png",
    [154]="icon_cloudy.png",
    [900]="icon_sunny.png",
    [901]="icon_snow.png",
    [1]="icon_duoyun.png",
    [3]="icon_rain.png",
    [4]="icon_snow.png",
    [5]="icon_fog.png",
    [8]="icon_sunny_night.png",
}

local function getweatherImg(iconCode)
	iconCode=tonumber(iconCode)
	return "/lua/"..(weatherImg[iconCode]~=nil and weatherImg[iconCode] or (weatherImg[math.floor(iconCode/100)] ~= nil and weatherImg[math.floor(iconCode/100)] or "icon_cloudy.png"))
end

local function cbFnc(result,prompt,head,body)
    if result and body then
        _G.weatherTable,result,errinfo=json.decode(body)
        print(body)
        sys.publish("weatherChanged")
    end
end

function weatherService(img,temp,text)

    sys.taskInit(function()
        while _G.weatherTable.code ~="200" do sys.wait(500) end
        local lastTime=0
        while _G.HOME do
            if img== _G.skyimg and temp==_G.TEMPLabel and text==_G.weatherTextLabel then
                if lastTime~=_G.weatherTable.updateTime then 
                    lastTime=_G.weatherTable.updateTime
                    lvgl.img_set_src(img,getweatherImg(_G.weatherTable.now.icon))
                    lvgl.label_set_text(temp, "#FFFFFF ".._G.weatherTable.now.temp)
                    lvgl.label_set_text(text, string.format("#FFFFFF %s %s%s级",_G.weatherTable.now.text,_G.weatherTable.now.windDir,_G.weatherTable.now.windScale))
                end
                sys.wait(5000)
            else
                break
            end
        end
    end)
end

sys.taskInit(function()
    log.info("[天气任务]","启动")
    while true do
        while service_location.getLocation().result ~=0 do sys.wait(500) end
        log.info("[天气任务]","拉天气")
        local addr= service_location.getLocation()
        local lat =addr.lat
        local lng =addr.lng
        http.request("GET","https://devapi.qweather.com/v7/weather/now?key=ea3f37dbc7d6443b90acb377a0c6c31f&location="..lng..","..lat.."&lang=zh&unit=m&gzip=n",nil,nil,nil,nil,cbFnc)
        sys.wait(1000*60*15)
    end
end)
