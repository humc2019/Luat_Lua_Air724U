module(..., package.seeall)
----NS2009

require "misc"
require "utils"
require "pins"

NeedCalibration=true
local i2cid = 2
local ns2009addr = 0x48
local SCREEN_X_PIXEL=480
local SCREEN_Y_PIXEL=854

pxtable={
    x1=32,
    y1=32,
    x2=448,
    y2=822,
    kx=0.14035087719298,
    ky=0.22048562656991,
    bx= -33.684210526316,
    by= -4.1596427574658,
}

local ispress = false
local last_x, last_y
x = 0
y = 0


function init()
    if i2c.setup(i2cid, i2c.SLOW) ~= i2c.SLOW then
        print("i2c.init fail")
    end
    if io.exists("lua/screenCLB.x") then
        local s=json.decode(io.readFile("/lua/screenCLB.x"))
        pxtable.by=s.by
        pxtable.bx=s.bx
        pxtable.ky=s.ky
        pxtable.kx=s.kx
    end
end

function getdata()
    local x,y,z
    i2c.send(i2cid, ns2009addr, 0xe4)
    z = i2c.recv(i2cid, ns2009addr, 2)
    _, z = pack.unpack(z, ">H")
    z=bit.rshift(z,4)

    i2c.send(i2cid, ns2009addr, 0xc4)
    x=i2c.recv(i2cid, ns2009addr, 2)
    _, x = pack.unpack(x, ">H")
    x=bit.rshift(x,4)

    i2c.send(i2cid, ns2009addr, 0xd4)
    y=i2c.recv(i2cid, ns2009addr, 2)
    _, y = pack.unpack(y, ">H")
    y=bit.rshift(y,4)

    return z,x,y
end

function get()
    local pressed,x1,y1=getdata()
    if pressed <=100 then
        if ispress == false then
            return false, false, -1, -1 
        end

        ispress = false
        return true, ispress, x, y
    end
    x = x1*pxtable.kx+pxtable.bx
    y = y1*pxtable.ky+pxtable.by
    if ispress == true and last_x == x and last_y == y then
        return false, false, -1, -1
    end
    ispress = true
    last_x = x
    last_y = y
    -- log.info("ispress x,y ", ispress, x, y)
    return true, ispress, x, y
    -- end
end


local data = {type = lvgl.INDEV_TYPE_POINTER}

function input()

    if lvgl.indev_get_emu_touch then
        return lvgl.indev_get_emu_touch()
    end

    -- pmd.sleep(100)
    local ret, ispress, px, py = tp.get()
    if ret then
        if lastispress == ispress and lastpx == px and lastpy == py then
            return data
        end
        lastispress = ispress
        lastpx = px
        lastpy = py
        if ispress then
            tpstate = lvgl.INDEV_STATE_PR
        else
            tpstate = lvgl.INDEV_STATE_REL
        end
    else
        return data
    end
    data.state = tpstate
    data.point = {x = px, y = py}
    return data
end

function test()--校准
    local press=false
    local lastx=0
    local lasty=0
    print("进入测量")
    while getdata()<100 do
        sys.wait(5)
    end
    local z,x,y=getdata()

    while getdata()>100 do
        sys.wait(5)
    end

    sys.publish("TEST_END",x,y)
    
end


function calibration(fnc)
    sys.taskInit(function(t)
        local logicTable={
            x1=0,
            y1=0,
            x2=0,
            y2=0
        }
        -- 创建图片控件
        local img = lvgl.img_create(lvgl.scr_act(), nil)
        -- 设置图片显示的图像
        lvgl.img_set_src(img, "/lua/px.bin")

        lvgl.obj_align(img, nil, lvgl.ALIGN_IN_TOP_RIGHT, 0, 0)
        test()
        _, x,y = sys.waitUntil("TEST_END", 120000)
        logicTable.x1=x
        logicTable.y1=y

        lvgl.obj_align(img, nil, lvgl.ALIGN_IN_BOTTOM_LEFT, 0, 0)
        test()
        _, x,y = sys.waitUntil("TEST_END", 120000)
        logicTable.x2=x
        logicTable.y2=y

        t.kx=(t.x2-t.x1)/(logicTable.x2-logicTable.x1)
        t.ky=(t.y2-t.y1)/(logicTable.y2-logicTable.y1)
        t.bx=t.x1-(t.kx*logicTable.x1)
        t.by=t.y1-(t.ky*logicTable.y1)
        local data={
            kx=t.kx,
            ky=t.ky,
            bx=t.bx,
            by=t.by
        }
        io.writeFile("/lua/screenCLB.x",json.encode(data))
        lvgl.obj_del(img)
        if fnc ~=nil then fnc() end
    end,pxtable)
end


sys.taskInit(init)
