PROJECT = "components"
VERSION = "1.0.1"

PRODUCT_KEY = "PA916VPSUsSQ4TcSCfK11sZGaBzgQ3tM"

require "mipi_lcd_GC9503"

require "tp"

lvgl.init(function()

    -- lvgl.indev_get_emu_touch and _G.LCD_W, _G.LCD_H = lvgl.disp_get_lcd_info() or _G.LCD_H, _G.LCD_W = lvgl.disp_get_lcd_info()

    if lvgl.indev_get_emu_touch then
        _G.LCD_W, _G.LCD_H = lvgl.disp_get_lcd_info()
    else
        lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270)
        _G.LCD_H, _G.LCD_W = lvgl.disp_get_lcd_info()
    end

    spi.setup(spi.SPI_1, 1, 1, 8, 5000000, 1)
    -- io.mount(io.SDCARD)
    -- tp.input
end, tp.input)

require "chs_keyborard"
chs_keyborard.create()

sys.init(0, 0)
sys.run()
