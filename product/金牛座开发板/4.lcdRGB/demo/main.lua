PROJECT = "rgb_demo"
VERSION = "1.0.1"

PRODUCT_KEY = "PA916VPSUsSQ4TcSCfK11sZGaBzgQ3tM"

require "log"
require "sys"
require "http"
require "misc"
require "pins"
-- require "mipi_lcd_GC9503"
require "mipi"
require "tp"

require "config"
require "nvm"
require "symbol"

nvm.init("config.lua")

lvgl.init(function()
    lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270)
    print(lvgl.disp_get_rotation(nil))
    spi.setup(spi.SPI_1, 1, 1, 8, 5000000, 1)
    -- _G.LCD_W, _G.LCD_H = lvgl.disp_get_lcd_info()
    _G.LCD_H, _G.LCD_W = lvgl.disp_get_lcd_info()
    -- log.info("lvgl, info", WIDTH, HEIGHT)
end, tp.input)

require "components"

arc.create(lvgl.scr_act(), {
    R = 360,
    align = lvgl.ALIGN_CENTER,
    click = false,
    adjustable = true,
    rotation = 270,
    bg_end = 360,
    value = 90,
    style = {
        border = {color = 0x0ff00f, width = 10, opa = 200},
        shadow = {spread = 10, width = 10, color = 0xff00f0},
        line = {color = 0xafccfc, width = 30, opa = 255, rounded = false},
        outline = {color = 0xff0000, opa = 100, width = 20},
        bg = {
            radius = 50,
            color = 0x0f0ff0,
            opa = 250,
            grad = {color = 0x0f0f0f, main = 60, grad = 300}
        }
    },
    style_indic = {color = 0xFFCCCC, width = 30, opa = 200, rounded = false},
    style_knob = {inner = 5}
})

sys.init(0, 0)
sys.run()
