module(..., package.seeall)

function create(p, v)
    obj = lvgl.calendar_create(p, nil)
    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end

    if v.today then
        today = lvgl.calendar_date_t()
        today.year = v.today.year
        today.month = v.today.month
        today.day = v.today.day
        lvgl.calendar_set_today_date(obj, today)
        lvgl.calendar_set_showed_date(obj, today)
    end

    if v.highlight_list then
        local highlightDate = {}
        for i = 1, #v.highlight_list do
            highlightDate[i] = lvgl.calendar_date_t()
            highlightDate[i].year = v.highlight_list[i].year
            highlightDate[i].month = v.highlight_list[i].month
            highlightDate[i].day = v.highlight_list[i].day
        end
        lvgl.calendar_set_highlighted_dates(obj, highlightDate, 1)
    end
    lvgl.calendar_set_day_names(obj, v.days)
    lvgl.calendar_set_month_names(obj, v.months)

    -- 日历的主要部分称为 lvgl.CALENDAR_PART_BG 。它使用典型的背景样式属性绘制背景。
    -- 除以下虚拟部分外：
    -- lvgl.CALENDAR_PART_HEADER 显示当前年和月名称的上部区域。它还具有用于移动下一个/上个月的按钮。
    -- 它使用典型的背景属性以及填充来调整其大小和边距，以设置距日历顶部的距离和日历下方的日期。
    -- lvgl.CALENDAR_PART_DAY_NAMES 在标题下方显示日期名称。它使用文本样式属性填充来与背景（左，右），标题（上）和日期（下）
    -- 保持一定距离。
    -- lvgl.CALENDAR_PART_DATE 显示从1..28 / 29/30/31开始的日期数字（取决于当月）。
    -- 根据本部分中定义的状态来绘制状态的不同“状态”：
    -- 正常日期：以 lvgl.STATE_DEFAULT 样式绘制
    -- 按日期范围：以 lvgl.STATE_PRESSED 样式绘制
    -- 今天：以 lvgl.STATE_FOCUSED 样式绘制
    -- 高亮显示的日期：以 lvgl.STATE_CHECKED 样式绘制
    -- 其他：以 lvgl.STATE_DISABLED 样式绘制

    if v.style then
        lvgl.obj_add_style(obj, lvgl.CALENDAR_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.CALENDAR_PART_HEADER,
                           create_style(v.style.header))
        lvgl.obj_add_style(obj, lvgl.CALENDAR_PART_DAY_NAMES,
                           create_style(v.style.day_names))
        lvgl.obj_add_style(obj, lvgl.CALENDAR_PART_DATE,
                           create_style(v.style.date))
    end

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    return obj
end
local calendar_data = {
    today = {year = 2022, month = 4, day = 8},
    highlight_list = {
        {year = 2022, month = 4, day = 9}, {year = 2022, month = 4, day = 11},
        {year = 2022, month = 4, day = 28}
    },
    days = {"#日", "mon", "二", "周三", "四", "星期五", "六", ""},
    months = {
        "测试一月", "测试二月", "测试三月", "测试四月",
        "测试五月", "测试六月", "测试七月", "测试8月",
        "测试9月", "测试10月", "测试11月", "测试十二月", ""
    },
    style = {
        bg = {
            outline = {width = 0},
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            bg = {radius = 10, color = 0xffff00, grad = {color = 0x0f0f0f}},
            text = {font = style.font24}
        },
        header = {
            bg = {radius = 10, color = 0x00ff00, grad = {color = 0x0f0f0f}},
            text = {font = style.font24, color = 0xff00ff}
        },
        day_names = {
            bg = {radius = 10, color = 0x0000ff, grad = {color = 0x0f0f0f}},
            text = {font = style.font24, color = 0x00ffff}
        },
        date = {
            bg = {radius = 10, color = 0x00ffff, grad = {color = 0x0f0f0f}},
            text = {font = style.font24}
        }
    }
}
-- create(lvgl.scr_act(), calendar_data)

