module(..., package.seeall)

function create(p, v)
    local a = lvgl.arc_create(p, nil)

    -- 弧可以具有不同的“类型”。它们用 lvgl.arc_set_type 设置。存在以下类型：
    -- lvgl.ARC_TYPE_NORMAL 指示器弧顺时针绘制（最小电流）
    -- lvgl.ARC_TYPE_REVERSE 指示器弧沿逆时针方向绘制（最大电流）
    -- lvgl.ARC_TYPE_SYMMETRIC 从中间点绘制到当前值的指示弧。
    lvgl.arc_set_type(a, v.type or lvgl.ARC_TYPE_NORMAL)

    lvgl.obj_set_click(a, v.click or false)

    lvgl.obj_set_size(a, v.R or 100, v.R or 100)

    -- 偏移角度，默认开始角度在三点钟方向，通过这个值修改弧线的起点
    lvgl.arc_set_rotation(a, v.rotation or 0)

    -- 背景角度   注意：要先绘制背景！！！
    lvgl.arc_set_bg_angles(a, v.bg_start or 0, v.bg_end or 360)
    -- 前景角度
    lvgl.arc_set_angles(a, v.arc_start or 0, v.arc_end or 0)

    lvgl.arc_set_range(a, v.range_s or 0, v.range_e or 100)
    lvgl.arc_set_value(a, v.value or 80)

    lvgl.arc_set_adjustable(a, v.adjustable or false)

    lvgl.obj_align(a, v.align_to, v.align or lvgl.ALIGN_IN_TOP_MID,
                   v.align_x or 0, v.align_y or 0)

    -- lvgl.obj_set_style_local_line_rounded(a, lvgl.ARC_PART_INDIC, lvgl.STATE_DEFAULT, 0)

    -- 弧的主要部分称为 lvgl.ARC_PART_BG 。它使用典型的背景样式属性绘制背景，并使用线型属性绘制圆弧。
    -- 圆弧的大小和位置将遵守填充样式的属性。
    -- lvgl.ARC_PART_INDIC 是虚拟零件，它使用线型属性绘制另一个弧。它的填充值是相对于背景弧线解释的。
    -- 指示器圆弧的半径将根据最大填充值进行修改。
    -- lvgl.ARC_PART_KNOB 是虚拟零件，它绘制在弧形指示器的末端。它使用所有背景属性和填充值。
    -- 使用零填充时，旋钮的大小与指示器的宽度相同。较大的填充使其较大，较小的填充使其较小。

    lvgl.obj_add_style(a, lvgl.ARC_PART_BG, create_style(v.style.bg))
    lvgl.obj_add_style(a, lvgl.ARC_PART_INDIC, create_style(v.style.indic))
    lvgl.obj_add_style(a, lvgl.ARC_PART_KNOB, create_style(v.style.knob))

    return a
end
arc_data = {
    R = 360,
    align = lvgl.ALIGN_CENTER,
    click = true,
    adjustable = true,
    rotation = 270,
    bg_end = 360,
    value = 90,
    style = {
        bg = {
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            bg = {radius = 10, color = 0x00ff00, grad = {color = 0x0f0f0f}},
            line = {color = 0xCCFFCC, width = 30, opa = 200, rounded = false},
            transform = {angle = 4500, zoom = 512}
        },
        indic = {
            bg = {radius = 10, color = 0x00ff00, grad = {color = 0x0f0f0f}},
            line = {color = 0xFFCCCC, width = 30, opa = 200, rounded = false}
        },
        knob = {
            bg = {
                radius = 10,
                color = 0x00ff00,
                opa = 0,
                grad = {color = 0x0f0f0f}
            },
            pad = {all = -100}
        }
    }

}
-- create(lvgl.scr_act(), arc_data)
