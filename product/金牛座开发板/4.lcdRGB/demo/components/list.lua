module(..., package.seeall)
function create(p, v)
    obj = lvgl.list_create(p, nil)
    if v.click then lvgl.obj_set_click(obj, true) end
    lvgl.obj_set_size(obj, v.W or 400, v.H or 200)
    local btn_list = {}
    for i = 1, #v.btns do
        btn_list[i] = lvgl.list_add_btn(obj, lvgl.SYMBOL_NEW_LINE, v.btns[i],
                                        v.btns[i].click)
        lvgl.obj_add_style(btn_list[i], lvgl.BTN_PART_MAIN,
                           create_style(v.style.btn))
    end

    -- 列表与 页面(Page) 具有相同的部分
    -- lvgl.LIST_PART_BG 
    -- lvgl.LIST_PART_EDGE_FLASH
    -- lvgl.LIST_PART_SCROLLABLE 
    -- lvgl.LIST_PART_SCROLLBAR 
    -- 有关详细信息，请参见 页面(Page) 部分。
    -- 列表上的按钮被视为普通按钮，它们只有一个主要部分，称为 lvgl.BTN_PART_MAIN 。
    -- 列表上的按钮可以有边缘，称为 lvgl.BTN_PART_EDGE 。

    if v.style then
        lvgl.obj_add_style(obj, lvgl.LIST_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.LIST_PART_SCROLLBAR,
                           create_style(v.style.bar))
        lvgl.obj_add_style(obj, lvgl.LIST_PART_EDGE_FLASH,
                           create_style(v.style.edge_flash))
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_SCROLLABLE,
                           create_style(v.style.lable))

    end

    lvgl.obj_align(obj, v.align_to or nil, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
end
local list_data = {
    W = 200,
    H = 200,
    icon = {
        lvgl.SYMBOL_NEW_LINE, lvgl.SYMBOL_DIRECTORY, lvgl.SYMBOL_CLOSE,
        lvgl.SYMBOL_EDIT, lvgl.SYMBOL_USB, lvgl.SYMBOL_GPS, lvgl.SYMBOL_STOP,
        lvgl.SYMBOL_VIDEO
    },
    btns = {
        "星期一", "Open", "Delete", "Edit", "USB", "GPS", "Stop", "Video",
        "Stop", "Video", "Stop", "Video", "Stop", "Video"
    },
    style = {
        bg = {bg = {color = 0xff0000}},
        btn = {
            bg = {color = 0x00ffff},
            text = {font = style.font24, color = 0xff0000}
        },
        lable = {bg = {color = 0x0000ff}},
        edge_flash = {bg = {color = 0x0000ff}},
        bar = {bg = {color = 0xcccccc}}
    }
}
-- create(lvgl.scr_act(), list_data)

