module(..., package.seeall)

font16 = lvgl.font_load(spi.SPI_1, 16, 2, 40)
font24 = lvgl.font_load(spi.SPI_1, 24, 2, 50)
font36 = lvgl.font_load(spi.SPI_1, 36, 2, 60)
font48 = lvgl.font_load(spi.SPI_1, 48, 2, 96)
font68 = lvgl.font_load(spi.SPI_1, 50, 2, 125)
font96 = lvgl.font_load(spi.SPI_1, 96, 1, 150)
font110 = lvgl.font_load(spi.SPI_1, 110, 1, 160)

font120 = lvgl.font_load(spi.SPI_1, 120, 1, 180)

