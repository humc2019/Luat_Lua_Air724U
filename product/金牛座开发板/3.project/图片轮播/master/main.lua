PROJECT = "imgandmusic"
VERSION = "9.9.9"

PRODUCT_KEY = "PA916VPSUsSQ4TcSCfK11sZGaBzgQ3tM"


require "log"
require "sys"
require "http"
require "misc"
require "pins"
require "mipi_lcd_GC9503"
-- require "mipi_lcd_ILI9806E"
-- require "mipi_lcd_ILI9806E_me"
require "tp"

lvgl.init(function()
    if not lvgl.indev_get_emu_touch then lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270) end
end, tp.input)


require "activity"

activity.init()

require "ProjectSwitch"

sys.init(0, 0)
sys.run()
