module(..., package.seeall)


function create_bg_style(v)
    if not v then v = {} end

    local s = lvgl.style_t()

    lvgl.style_init(s)

    if not v then return s end
    -- 设置背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 设置边框
    if v.border then s = set_border(s, v.border) end
    -- 设置外框线
    if v.outline then s = set_outline(s, v.outline) end
    -- 设置阴影
    if v.shadow then s = set_shadow(s, v.shadow) end
    -- 设置背景线样式
    if v.line then s = set_line(s, v.line) end

    return s
end

function create_knob_style(v)
    if not v then v = {} end

    local s = lvgl.style_t()

    lvgl.style_init(s)
    if not v then return s end

    s = set_pad(s, v)
    return s

end

function create_indic_style(v)
    if not v then v = {} end

    local s = lvgl.style_t()

    lvgl.style_init(s)
    if not v then return s end

    -- line_color ( lvgl.color_t )：线条的颜色。默认值：lvgl.COLOR_BLACK
    -- line_opa ( lvgl.opa_t )：直线的不透明度。默认值：lvgl.OPA_COVER
    -- line_width ( lvgl.style_int_t )：线的宽度。默认值：0。
    -- line_dash_width ( lvgl.style_int_t )：破折号的宽度。仅对水平或垂直线绘制虚线。0：禁用破折号。默认值：0。
    -- line_dash_gap ( lvgl.style_int_t )：两条虚线之间的间隙。仅对水平或垂直线绘制虚线。0：禁用破折号。默认值：0。
    -- line_rounded ( bool )：true：绘制圆角的线尾。默认值：false。
    -- line_blend_mode ( lvgl.blend_mode_t )：设置线条的混合模式。可以是 lvgl.BLEND_MODE_NORMAL/ADDITIVE/SUBTRACTIVE。默认值：lvgl.BLEND_MODE_NORMAL。

    -- Write style state: lvgl.STATE_DEFAULT for s
    lvgl.style_set_line_color(s, lvgl.STATE_DEFAULT,
                              lvgl.color_hex(v.color or 0xFFFFFF))
    lvgl.style_set_line_width(s, lvgl.STATE_DEFAULT, v.width or 5)
    lvgl.style_set_line_opa(s, lvgl.STATE_DEFAULT, v.opa or 255)

    -- lvgl.style_set_line_rounded(&style, lvgl.STATE_DEFAULT, true)
    lvgl.style_set_line_rounded(s, lvgl.STATE_DEFAULT, v.rounded)
    -- lvgl.style_set_line_dash_gap()
    -- lvgl.style_set_line_dash_width()
    -- lvgl.style_set_line_blend_mode()

    return s
end

function create(p, v)
    local a = lvgl.arc_create(p, nil)

    -- 弧可以具有不同的“类型”。它们用 lvgl.arc_set_type 设置。存在以下类型：
    -- lvgl.ARC_TYPE_NORMAL 指示器弧顺时针绘制（最小电流）
    -- lvgl.ARC_TYPE_REVERSE 指示器弧沿逆时针方向绘制（最大电流）
    -- lvgl.ARC_TYPE_SYMMETRIC 从中间点绘制到当前值的指示弧。
    lvgl.arc_set_type(a, v.type or lvgl.ARC_TYPE_NORMAL)

    lvgl.obj_set_click(a, v.click or true)

    lvgl.obj_set_size(a, v.R or 100, v.R or 100)

    -- 偏移角度，默认开始角度在三点钟方向，通过这个值修改弧线的起点
    lvgl.arc_set_rotation(a, v.rotation or 0)

    -- 背景角度   注意：要先绘制背景！！！
    lvgl.arc_set_bg_angles(a, v.bg_start or 0, v.bg_end or 360)
    -- 前景角度
    lvgl.arc_set_angles(a, v.arc_start or 0, v.arc_end or 0)

    lvgl.arc_set_range(a, v.range_s or 0, v.range_e or 100)
    lvgl.arc_set_value(a, v.value or 80)

    lvgl.arc_set_adjustable(a, v.adjustable or false)

    lvgl.obj_align(a, v.align_to, v.align or lvgl.ALIGN_IN_TOP_MID,
                   v.align_x or 0, v.align_y or 0)

    -- lvgl.obj_set_style_local_line_rounded(a, lvgl.ARC_PART_INDIC, lvgl.STATE_DEFAULT, 0)

    lvgl.obj_add_style(a, lvgl.ARC_PART_BG, create_bg_style(v.style))

    lvgl.obj_add_style(a, lvgl.ARC_PART_INDIC, create_indic_style(v.style_indic))
    lvgl.obj_add_style(a, lvgl.ARC_PART_KNOB, create_knob_style(v.style_knob))

    return a
end
-- create(lvgl.scr_act(), {
--     R = 360,
--     align = lvgl.ALIGN_CENTER,
--     click = false,
--     adjustable = true,
--     rotation = 270,
--     bg_end = 360,
--     value = 90,
--     style = {
--         border = {color = 0x0ff00f, width = 10, opa = 200},
--         shadow = {spread = 10, width = 10, color = 0xff00f0},
--         line = {color = 0xafccfc, width = 30, opa = 255, rounded = false},
--         outline = {color = 0xff0000, opa = 100, width = 20},
--         bg = {
--             radius = 50,
--             color = 0x0f0ff0,
--             opa = 250,
--             grad = {color = 0x0f0f0f, main = 60, grad = 300}
--         }
--     },
--     style_indic = {color = 0xFFCCCC, width = 30, opa = 200, rounded = false},
--     style_knob = {inner = 5}
-- })
