module(..., package.seeall)

function create_bg_style(v)
    if not v then v = {} end

    local s = lvgl.style_t()

    lvgl.style_init(s)

    lvgl.style_set_bg_color(s, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.bg_color or 0xFFFFFF))
    lvgl.style_set_bg_opa(s, lvgl.STATE_DEFAULT, v.bg_opa or 255)

    lvgl.style_set_border_color(s, lvgl.STATE_DEFAULT,
                                lvgl.color_hex(v.border_color or 0xFFFFFF))

    lvgl.style_set_border_width(s, lvgl.STATE_DEFAULT, v.border_width or 1)
    lvgl.style_set_border_opa(s, lvgl.STATE_DEFAULT, v.border_opa or 255)

    return s
end

function create_indic_style(v)
    if not v then v = {} end

    local s = lvgl.style_t()

    lvgl.style_init(s)

    lvgl.style_set_bg_color(s, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.bg_color or 0xFFFFFF))
    lvgl.style_set_bg_opa(s, lvgl.STATE_DEFAULT, v.bg_opa or 255)

    lvgl.style_set_border_color(s, lvgl.STATE_DEFAULT,
                                lvgl.color_hex(v.border_color or 0xFFFFFF))
    lvgl.style_set_border_width(s, lvgl.STATE_DEFAULT, v.border_width or 1)
    lvgl.style_set_border_opa(s, lvgl.STATE_DEFAULT, v.border_opa or 255)

    return s
end

-- 进度条的主要部分称为 LV_BAR_PART_BG ，它使用典型的背景样式属性。
-- LV_BAR_PART_INDIC 是一个虚拟部件，还使用了所有典型的背景属性。
-- 默认情况下，指示器的最大尺寸与背景的尺寸相同，
-- 但是在其中设置正的填充值 LV_BAR_PART_BG 将使指示器变小。
-- （负值会使它变大）如果在指标上使用了值样式属性，
-- 则将根据指标的当前大小来计算对齐方式。
-- 例如，中心对齐的值始终显示在指示器的中间，而不管其当前大小如何。

function create(p, v)
    local b = lvgl.bar_create(p, nil)

    -- 动画的时间以毫秒为单位。先设置动画时间，才能生效
    lvgl.bar_set_anim_time(b, v.anim_time or 10000)
    -- 设置进度条的范围，默认是1-100
    lvgl.bar_set_range(b, v.range_s or 0, v.range_e or 240)

    -- 设置进度条的值
    lvgl.bar_set_value(b, v.value or 10, v.value_anim or lvgl.ANIM_ON)

    -- -- 设置进度条的起始值
    -- lvgl.bar_set_start_value(b, v.start_value or 0,
    --                          v.start_value_anim or lvgl.ANIM_ON)

    -- BAR_TYPE_CUSTOM
    -- BAR_TYPE_NORMAL
    -- BAR_TYPE_SYMMETRICAL 对称地绘制为零（从零开始，从左至右绘制）
    lvgl.bar_set_type(b, v.type or lvgl.BAR_TYPE_NORMAL)

    lvgl.obj_set_size(b, v.W or 400, v.H or 40)
    lvgl.obj_align(b, v.align_to, v.align or lvgl.ALIGN_CENTER, v.align_x or 0,
                   v.align_y or 0)

    if v.bg_style then
        lvgl.obj_add_style(b, lvgl.BAR_PART_BG, create_bg_style(v.bg_style))
    end

    if v.indic_style then
        lvgl.obj_add_style(b, lvgl.BAR_PART_INDIC,
                           create_indic_style(v.indic_style))
    end
    return b
end

-- create(lvgl.scr_act(), {
--     range_s = 0,
--     range_e = 280,
--     value = 180,
--     anim_time = 1000000,
--     indic_style = {bg_color = 0xF0F000, border_width = 0},
--     bg_style = {bg_color = 0xFF0000, border_width = 0}
-- })
