module(..., package.seeall)

_G.parent_cont = cont.create(lvgl.scr_act(), {
    W = LCD_W,
    H = LCD_H,
    click = false,
    style = {border = {width = 0}}
})

_G.left_cont = cont.create(parent_cont, {
    W = LCD_W / 3,
    H = LCD_H,
    align = lvgl.ALIGN_IN_LEFT_MID,
    click = false,
    style = {border = {width = 0}}
})

img_ = img.create(left_cont, {
    -- align = lvgl.ALIGN_IN_LEFT_MID,
    src = "/lua/l2.jpg"
})

_G.right_cont = cont.create(parent_cont, {
    W = LCD_W * 2 / 3,
    H = LCD_H,
    align = lvgl.ALIGN_IN_RIGHT_MID,
    click = false,
    style = {border = {width = 0}, bg = {color = 0xccfcfc}}
})

_G.back_home = function(obj, event)
    if (event == lvgl.EVENT_CLICKED) then home_page.create() end
end

img_data = {"l1.jpg", "l2.jpg", "l3.jpg"}

sys.timerLoopStart(function()
    local index = os.date("*t").sec % 3 + 1
    lvgl.img_set_src(img_, "/lua/l" .. index .. ".jpg")
end, 2000)

require "home_page"
require "pick_up_page"
require "choose_page"
require "help_page"

-- help_page.create()
home_page.create()
-- pick_up_page.create()
-- choose_page.create()

create_msg = function(p, v)
    back_btn = btn.create(p, {
        W = 124,
        H = 48,
        align = lvgl.ALIGN_IN_BOTTOM_MID,
        align_y = -10,
        event = function(obj, event)
            if (event == lvgl.EVENT_CLICKED) then
                lvgl.obj_del(shadow_cont)
            end
        end,
        label = {
            text = "返回",
            style = {
                text = {
                    font = style.font24,
                    color = 0xffffff,
                    letter_space = 20,
                    line_space = 20
                }
            }
        },
        style = {
            border = {width = 0},
            shadow = {spread = 10, color = 0xff00f0},
            bg = {radius = 50, color = 0xff9999}
        }
    })
    tips_label = label.create(p, {
        text = v.tips_label,
        align_to = back_btn,
        align = lvgl.ALIGN_OUT_TOP_MID,
        align_y = -10,
        style = {text = {font = style.font24}}
    })

    state_label = label.create(p, {
        text = v.state_label,
        align_to = tips_label,
        align = lvgl.ALIGN_OUT_TOP_MID,
        align_y = -10,
        style = {text = {font = style.font36}}
    })

    state_img = img.create(p, {
        align_to = state_label,
        align = lvgl.ALIGN_OUT_TOP_MID,
        align_y = -30,
        style = {recolor = 0xff0000},
        src = "/lua/" .. v.src .. ".bin"
    })

end

