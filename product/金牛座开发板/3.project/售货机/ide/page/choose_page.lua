module(..., package.seeall)
function create(v)
    if not v then v = {} end
    lvgl.obj_clean(right_cont)
    left_img_cont = cont.create(right_cont, {
        W = LCD_W * 4 / 18,
        H = LCD_H * 47 / 48,
        align = lvgl.ALIGN_IN_LEFT_MID,
        align_x = LCD_W / 27,
        click = false,
        style = {border = {width = 0}, bg = {radius = 30}}
    })

    img_choose = img.create(left_img_cont, {src = "/lua/l1.jpg"})

    right_pay_cont = cont.create(right_cont, {
        W = LCD_W * 13 / 36,
        H = LCD_H * 47 / 48,
        align = lvgl.ALIGN_OUT_RIGHT_MID,
        align_x = 10,
        align_to = left_img_cont,
        click = false,
        style = {border = {width = 0}, bg = {radius = 20}}
    })

    goods_info_data = v.goods_info_data or
                          {
            src = "kele",
            price = "3.00",
            name = "百事可乐 250ML"
        }

    goods_info_cont = cont.create(right_pay_cont, {
        W = LCD_W / 4,
        H = LCD_H * 21 / 48,
        align = lvgl.ALIGN_IN_TOP_MID,
        click = false,
        style = {border = {width = 0}, bg = {radius = 0, color = 0xffffff}}
    })

    goods_img = img.create(goods_info_cont, {
        align = lvgl.ALIGN_IN_TOP_MID,
        align_y = 35,
        -- zoom = 512,
        src = "/lua/" .. goods_info_data.src..".jpg"
    })

    price_label = label.create(goods_info_cont, {
        text = "#ff0000 " .. goods_info_data.price .. "元",
        align = lvgl.ALIGN_IN_BOTTOM_LEFT,
        recolor = true,
        align_x = 10,
        align_y = -10,
        style = {text = {font = style.font24}}
    })

    name_label = label.create(goods_info_cont, {
        text = goods_info_data.name,
        align = lvgl.ALIGN_OUT_TOP_LEFT,
        align_to = price_label,
        align_y = -5,
        style = {text = {font = style.font24}}
    })

    back_cont = cont.create(right_pay_cont, {
        W = LCD_W * 6 / 54,
        H = LCD_W * 6 / 108,
        align = lvgl.ALIGN_IN_TOP_RIGHT,
        align_x = -5,
        align_y = 5,
        click = true,
        event = back_home,
        style = {
            border = {width = 0},
            shadow = {
                width = 10,
                spread = 1,
                opa = 60,
                color = 0,
                ofs_y = 2,
                ofs_x = 2
            },
            bg = {radius = 20, color = 0xffd264}
        }
    })

    back_img = img.create(back_cont, {
        align = lvgl.ALIGN_IN_TOP_MID,
        src = "/lua/back.bin",
        style = {recolor = 0xffffff}
    })

    clock_img = img.create(back_cont, {
        align = lvgl.ALIGN_IN_BOTTOM_LEFT,
        align_x = 15,
        src = "/lua/clock.bin",
        style = {recolor = 0xffffff}
    })

    auto_back_time_label = label.create(back_cont, {
        text = "113秒",
        align = lvgl.ALIGN_OUT_RIGHT_MID,
        align_to = clock_img,
        align_x = 5,
        style = {text = {font = style.font14, color = 0xffffff}}
    })

    qrcode_cont = cont.create(right_pay_cont, {
        W = LCD_H * 16 / 48,
        H = LCD_H * 16 / 48,
        align = lvgl.ALIGN_OUT_BOTTOM_MID,
        align_to = goods_info_cont,
        click = false,
        style = {border = {width = 3, color = 0xccffcc}, bg = {radius = 5}}
    })

    qrcode.create(qrcode_cont, {
        W = (LCD_H * 16 / 48) - 5,
        H = (LCD_H * 16 / 48) - 5,
        text = "请打钱"
    })

    qrcode_tips_label = label.create(right_pay_cont, {
        text = "请扫描上方二维码进行支付",
        align = lvgl.ALIGN_OUT_BOTTOM_MID,
        align_to = qrcode_cont,
        style = {text = {font = style.font18}}
    })

    pay_choice_cont = cont.create(right_pay_cont, {
        W = LCD_W * 12 / 36,
        H = LCD_H * 5 / 48,
        align = lvgl.ALIGN_IN_BOTTOM_MID,
        click = false,
        layout = lvgl.LAYOUT_PRETTY_MID,
        style = {
            border = {width = 0},
            -- bg = {radius = 0, opa = 100, color = 0xffffff},
            pad = {left = 55, right = 55, top = 0, bottom = 0}
        }
    })
    tips_label = label.create(right_pay_cont, {
        text = "本设备支持以下支付方式",
        align = lvgl.ALIGN_OUT_TOP_MID,
        align_to = pay_choice_cont,
        align_y = -5,
        style = {text = {font = style.font14}}
    })

    pay_data = {
        {src = "wechat", text = "微信"},
        {src = "zhifubao", text = "支付宝"},
        {src = "yunshanfu", text = "云闪付"}
    }
    for i = 1, #pay_data do
        pay_data[i].cont = cont.create(pay_choice_cont, {
            W = LCD_H * 5 / 48,
            H = LCD_H * 5 / 48,
            click = false,
            style = {border = {width = 0}}
        })
        pay_data[i].img = img.create(pay_data[i].cont, {
            align = lvgl.ALIGN_IN_TOP_MID,
            src = "/lua/" .. pay_data[i].src .. ".jpg"
        })
        pay_data[i].label = label.create(pay_data[i].cont, {
            text = pay_data[i].text,
            align = lvgl.ALIGN_IN_BOTTOM_MID,
            align_y = -5,
            style = {text = {font = style.font14}}
        })
    end

end
