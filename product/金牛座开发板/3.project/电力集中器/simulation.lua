module(...,package.seeall)

require "sys"
require "misc"

local ReEnergy
local Temp
local AcEnergy
local CurNow
local Cur
local Vol
local AcPower
local RePower
local Time
local Switch

local Month
local Day
local Hour
local Min
local Sec

local tVol = {"220","221","222","223"}  --当前电压

local tCur = {"10.0","10.1","10.3","10.4"}  --当前电流

local tCurNow = {"11","12","13","14"} --剩余电流

local tTemp = {"35.5","35.7","35.8","35.5"} --线路温度

local tAcPower = {"10.1","10.2","10.3","10.4"} --有功功率

local tRePower = {"10.1","10.3","10.2","10.3"}  --无功功率


local tSwitch = {"合","开","合","开"}  --闸状态

local tAcEnergy = {"10.1","10.2","10.3","10.4"} --有功电能

local tReEnergy = {"10.1","10.0","9.9","10.4"} --无功电能

sys.taskInit(function ()
    while true do
        for v = 1,4 do
            sys.publish("SIMULATION_RPT_RESULT",tVol[v],tCur[v],tCurNow[v],tTemp[v],tAcPower[v],tRePower[v],tSwitch[v],tAcEnergy[v],tReEnergy[v])
            print("Param",tVol[v],tCur[v],tCurNow[v],tTemp[v],tAcPower[v],tRePower[v],tSwitch[v],tAcEnergy[v],tReEnergy[v])

            sys.wait(5000)
            v = v + 1
            if v>=4 then
                v = 1
            end
        end
    end
end)

