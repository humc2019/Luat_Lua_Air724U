
--This function name and notes cannot be modified
--@@funCfg: <joinUs, exist>
function joinUs()
------------USER CODE DATA--------------
lvgl.style_set_bg_color(Style_LvglButton2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFF0000))
lvgl.style_set_value_str(Style_LvglButton2_1, lvgl.STATE_DEFAULT, "逗你的，还是手机扫码加群一起玩耍吧")
lvgl.style_set_value_color(Style_LvglButton2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0x1A1A1A))
lvgl.obj_add_style(ScreenA.LvglButton2.self, lvgl.BTN_PART_MAIN, Style_LvglButton2_1)
------------USER CODE DATA--------------
end


--This function name and notes cannot be modified
--@@funCfg: <reset, noexist>
function reset()
------------USER CODE DATA--------------
lvgl.style_set_bg_color(Style_LvglButton2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0x0088FF))
lvgl.style_set_value_str(Style_LvglButton2_1, lvgl.STATE_DEFAULT, "Hello LuatOS UI")
lvgl.obj_add_style(ScreenA.LvglButton2.self, lvgl.BTN_PART_MAIN, Style_LvglButton2_1)
lvgl.style_set_value_color(Style_LvglButton2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
------------USER CODE DATA--------------
end

--更新时间
sys.subscribe("SYSTEM_CHANGE_TIME",function(date)
    lvgl.label_set_text(ScreenA.LvglLabel2.self, date)
    end)

--更新4G信号
sys.subscribe("RSSI_CHANGE",function(img)
    lvgl.img_set_src(ScreenA.LvglImg2.self,img)
end)


--更新闸状态
sys.subscribe("SWITCH_CHANGE",function(status)
    lvgl.label_set_text(ScreenA.LvglLabel4.self,status)
end)

--更新数据
sys.subscribe("SIMULATION_RPT_RESULT", function(vol,cur,curNow,temp,acPower,rePower,switch,acEnergy,reEnergy)
    if ScreenA.LvglLabel20.self ~= nil then
        if cur and Cur~=cur then
            Cur = cur
            lvgl.label_set_text(ScreenA.LvglLabel20.self, Cur.."A")
        end
    end
end)

sys.subscribe("SIMULATION_RPT_RESULT", function(vol,cur,curNow,temp,acPower,rePower,switch,acEnergy,reEnergy)
    if ScreenA.LvglLabel19.self ~= nil then
        if vol and Vol~=vol then
            Vol = vol
            lvgl.label_set_text(ScreenA.LvglLabel19.self, Vol.."V")
        end
    end
end)

sys.subscribe("SIMULATION_RPT_RESULT", function(vol,cur,curNow,temp,acPower,rePower,switch,acEnergy,reEnergy)
    if ScreenA.LvglLabel21.self ~= nil then
        if curNow and CurNow~=curNow then
            CurNow = curNow
            lvgl.label_set_text(ScreenA.LvglLabel21.self, CurNow.."mA")
        end
    end
end)

sys.subscribe("SIMULATION_RPT_RESULT", function(vol,cur,curNow,temp,acPower,rePower,switch,acEnergy,reEnergy)
    if ScreenA.LvglLabel11.self ~= nil then
        if temp and Temp~=temp then
            Temp = temp
            lvgl.label_set_text(ScreenA.LvglLabel11.self, Temp.."°C")
        end
    end
end)

sys.subscribe("SIMULATION_RPT_RESULT", function(vol,cur,curNow,temp,acPower,rePower,switch,acEnergy,reEnergy)
    if ScreenA.LvglLabel14.self ~= nil then
        if acPower and AcPower~=acPower then
            AcPower = acPower
            lvgl.label_set_text(ScreenA.LvglLabel14.self, AcPower.."kW")
        end
    end
end)

sys.subscribe("SIMULATION_RPT_RESULT", function(vol,cur,curNow,temp,acPower,rePower,switch,acEnergy,reEnergy)
    if ScreenA.LvglLabel15.self ~= nil then
        if rePower and RePower~=rePower then
            RePower = rePower
            lvgl.label_set_text(ScreenA.LvglLabel15.self, RePower.."kW")
        end
    end
end)

sys.subscribe("SIMULATION_RPT_RESULT", function(vol,cur,curNow,temp,acPower,rePower,switch,acEnergy,reEnergy)
    if ScreenA.LvglLabel11.self ~= nil then
        if time and Time~=time then
            Time = time
            --lvgl.label_set_text(ScreenA.LvglLabel11.self, time)
        end
    end
end)

sys.subscribe("SIMULATION_RPT_RESULT", function(vol,cur,curNow,temp,acPower,rePower,switch,acEnergy,reEnergy)
    if ScreenA.LvglLabel4.self ~= nil then
        if switch and Switch~=switch then
            Switch = switch
            --lvgl.label_set_text(ScreenA.LvglLabel4.self, Switch)
        end
    end
end)

sys.subscribe("SIMULATION_RPT_RESULT", function(vol,cur,curNow,temp,acPower,rePower,switch,acEnergy,reEnergy)
    if ScreenA.LvglLabel6.self ~= nil then
        if acEnergy and AcEnergy~=acEnergy then
            AcEnergy = acEnergy
            lvgl.label_set_text(ScreenA.LvglLabel6.self, AcEnergy.."kW")
        end
    end
end)

sys.subscribe("SIMULATION_RPT_RESULT", function(vol,cur,curNow,temp,acPower,rePower,switch,acEnergy,reEnergy)
    if ScreenA.LvglLabel8.self ~= nil then
        if reEnergy and ReEnergy~=reEnergy then
            ReEnergy = reEnergy

            lvgl.label_set_text(ScreenA.LvglLabel8.self, ReEnergy.."kW")
        end
    end
end)
