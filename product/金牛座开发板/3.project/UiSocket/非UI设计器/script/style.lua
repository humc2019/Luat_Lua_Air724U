module(..., package.seeall)

    style_body = lvgl.style_t()
    lvgl.style_init(style_body)
    lvgl.style_set_bg_color(style_body, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x227bda))
    lvgl.style_set_radius(style_body, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_body, lvgl.CONT_PART_MAIN, 0)
    --透明
    -- lvgl.style_set_bg_opa(stytle_statusBox, lvgl.CONT_PART_MAIN,0)
------------------------------------------------------------------------------------------------------

    style_temp = lvgl.style_t()
    lvgl.style_init(style_temp)
    lvgl.style_set_bg_color(style_temp, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xff0000))
    lvgl.style_set_radius(style_temp, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_temp, lvgl.CONT_PART_MAIN, 0)

    lvgl.style_set_pad_left(style_temp, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_right(style_temp, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_top(style_temp, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_bottom(style_temp, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_inner(style_temp, lvgl.CONT_PART_MAIN,0)

    lvgl.style_set_margin_top(style_temp, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_margin_bottom(style_temp, lvgl.CONT_PART_MAIN,0)

------------------------------------------------------------------------------------------------------
    style_statusLabel = lvgl.style_t()
    lvgl.style_init(style_statusLabel)
    lvgl.style_set_bg_color(style_statusLabel, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x025bb5))
    lvgl.style_set_border_color(style_statusLabel,lvgl.CONT_PART_MAIN,lvgl.color_hex(0x025bb5))

------------------------------------------------------------------------------------------------------
    style_labelBgBlue = lvgl.style_t()
    lvgl.style_init(style_labelBgBlue)
    lvgl.style_set_bg_color(style_labelBgBlue, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x217bda))
    lvgl.style_set_text_color(style_labelBgBlue,lvgl.STATE_DEFAULT,lvgl.color_hex(0xFFFFFF))
    lvgl.style_set_border_width(style_labelBgBlue, lvgl.CONT_PART_MAIN, 0)
------------------------------------------------------------------------------------------------------
    style_labelBgBlue_noRadius = lvgl.style_t()
    lvgl.style_init(style_labelBgBlue_noRadius)
    lvgl.style_set_bg_color(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x217bda))
    lvgl.style_set_border_width(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_radius(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN, 0)
    -- lvgl.style_set_text_color(style_labelBgBlue_noRadius,lvgl.color_hex(0xffffff))

    lvgl.style_set_pad_left(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_right(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_top(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_bottom(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_inner(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,0)

------------------------------------------------------------------------------------------------------
    style_labelBgYellow = lvgl.style_t()
    lvgl.style_init(style_labelBgYellow)
    lvgl.style_set_bg_color(style_labelBgYellow, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xf0ff00))
    lvgl.style_set_border_width(style_labelBgYellow, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_radius(style_labelBgYellow, lvgl.CONT_PART_MAIN, 0)

------------------------------------------------------------------------------------------------------
    style_labelBgGreen = lvgl.style_t()
    lvgl.style_init(style_labelBgGreen)
    lvgl.style_set_bg_color(style_labelBgGreen, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x109b27))
    lvgl.style_set_border_width(style_labelBgGreen, lvgl.CONT_PART_MAIN, 0)

------------------------------------------------------------------------------------------------------
    style_labelBgRed = lvgl.style_t()
    lvgl.style_init(style_labelBgRed)
    lvgl.style_set_bg_color(style_labelBgRed, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xDC143C))
    lvgl.style_set_border_width(style_labelBgRed, lvgl.CONT_PART_MAIN, 0)
------------------------------------------------------------------------------------------------------
    style_labelBgSnowWhite = lvgl.style_t()
    lvgl.style_init(style_labelBgSnowWhite)
    lvgl.style_set_bg_color(style_labelBgSnowWhite, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xe9eaeb))
    lvgl.style_set_border_width(style_labelBgSnowWhite, lvgl.CONT_PART_MAIN, 0)
    -- lvgl.style_set_pad_bottom(style_labelBgSnowWhite, lvgl.CONT_PART_MAIN,20)

------------------------------------------------------------------------------------------------------
    style_btnWhite = lvgl.style_t()
    lvgl.style_init(style_btnWhite)
    lvgl.style_set_bg_color(style_btnWhite, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xffffff))
    lvgl.style_set_bg_color(style_btnWhite, lvgl.BTN_STATE_PRESSED,lvgl.color_hex(0x217bda))
    -- lvgl.style_set_border_width(style_btnWhite, lvgl.STATE_FOCUSED,0)
    lvgl.style_set_border_width(style_btnWhite, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_radius(style_btnWhite, lvgl.CONT_PART_MAIN, 5)
------------------------------------------------------------------------------------------------------
    style_dataBGWhite = lvgl.style_t()
    lvgl.style_init(style_dataBGWhite)
    lvgl.style_set_bg_color(style_dataBGWhite, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xe9eaeb))
    lvgl.style_set_border_width(style_dataBGWhite, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_radius(style_dataBGWhite, lvgl.CONT_PART_MAIN, 8)

    lvgl.style_set_pad_left(style_dataBGWhite, lvgl.CONT_PART_MAIN,5)
    lvgl.style_set_pad_right(style_dataBGWhite, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_top(style_dataBGWhite, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_bottom(style_dataBGWhite, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_inner(style_dataBGWhite, lvgl.CONT_PART_MAIN,5)
------------------------------------------------------------------------------------------------------
    style_switch = lvgl.style_t()
    lvgl.style_init(style_switch)
    lvgl.style_set_bg_color(style_switch, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x217bda))
    lvgl.style_set_border_width(style_switch, lvgl.CONT_PART_MAIN, 0)
    -- lvgl.style_set_radius(style_labelBgYellow, lvgl.CONT_PART_MAIN, 0)
------------------------------------------------------------------------------------------------------
    style_contentBox = lvgl.style_t()
    lvgl.style_init(style_contentBox)
    lvgl.style_set_bg_color(style_contentBox, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xd2e4f7))
    lvgl.style_set_radius(style_contentBox, lvgl.CONT_PART_MAIN, 20)
    lvgl.style_set_border_width(style_contentBox, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_margin_top(style_contentBox, lvgl.CONT_PART_MAIN, 0)
    -- 透明
    -- lvgl.style_set_bg_opa(style_contentBox, lvgl.CONT_PART_MAIN,95)
------------------------------------------------------------------------------------------------------
    style_msgBg = lvgl.style_t()
    lvgl.style_init(style_msgBg)
    lvgl.style_set_bg_color(style_msgBg, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xffffff))
    
    lvgl.style_set_radius(style_msgBg, lvgl.CONT_PART_MAIN, 10)
    lvgl.style_set_border_width(style_msgBg, lvgl.CONT_PART_MAIN, 0)
    -- lvgl.style_set_pad_left(style_msgBg, lvgl.CONT_PART_MAIN,0)
    -- lvgl.style_set_bg_opa(style_msgBg, lvgl.CONT_PART_MAIN,95)
    lvgl.style_set_pad_left(style_msgBg, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_right(style_msgBg, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_top(style_msgBg, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_bottom(style_msgBg, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_inner(style_msgBg, lvgl.CONT_PART_MAIN,0)
------------------------------------------------------------------------------------------------------
    style_QRbtnBg = lvgl.style_t()
    lvgl.style_init(style_QRbtnBg)
    lvgl.style_set_bg_color(style_QRbtnBg, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x025bb5))
    lvgl.style_set_radius(style_QRbtnBg, lvgl.CONT_PART_MAIN, 10)
    lvgl.style_set_border_width(style_QRbtnBg, lvgl.CONT_PART_MAIN, 0)
------------------------------------------------------------------------------------------------------
    style_divBox = lvgl.style_t()
    lvgl.style_init(style_divBox)
    lvgl.style_set_bg_opa(style_divBox, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_radius(style_divBox, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_divBox, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_pad_left(style_divBox, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_right(style_divBox, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_top(style_divBox, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_bottom(style_divBox, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_inner(style_divBox, lvgl.CONT_PART_MAIN,0)
----------------------------------------------------
    style_divBox1 = lvgl.style_t()
    lvgl.style_init(style_divBox1)
    lvgl.style_set_bg_opa(style_divBox1, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_radius(style_divBox1, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_divBox1, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_pad_left(style_divBox1, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_right(style_divBox1, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_top(style_divBox1, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_bottom(style_divBox1, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_inner(style_divBox1, lvgl.CONT_PART_MAIN,50)
------------------------------------------------------------------------------------------------------
    style_line = lvgl.style_t()
    lvgl.style_init(style_line)
    lvgl.style_set_bg_color(style_line, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x000000))
    lvgl.style_set_radius(style_line, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_line, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_margin_left(style_line, lvgl.CONT_PART_MAIN, 0)
------------------------------------------------------------------------------------------------------
    style_list = lvgl.style_t()
    lvgl.style_init(style_list)
    lvgl.style_set_bg_color(style_list, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xffffff))
    lvgl.style_set_radius(style_list, lvgl.CONT_PART_MAIN, 0)

    lvgl.style_set_margin_top(style_list, lvgl.CONT_PART_MAIN,5)

    lvgl.style_set_pad_left(style_list, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_right(style_list, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_top(style_list, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_bottom(style_list, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_inner(style_list, lvgl.CONT_PART_MAIN,0)

    lvgl.style_set_border_width(style_list, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_bg_opa(style_list, lvgl.CONT_PART_MAIN,0)
