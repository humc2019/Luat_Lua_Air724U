module(..., package.seeall)

valid_signal_style = create_style({
    bg = {
        radius = 0,
        color = 0x6cff8f
    }
})
invalid_signal_style = create_style({
    bg = {
        radius = 0,
        color = 0xcccccc
    }
})

function create(p, v)
    local signal_cont = cont.create(p or lvgl.scr_act(), v.main)
    color_ = 0x6cff8f
    if not v.num then
        v.num = 5
    end
    if not v.value then
        v.value = 0
    end
    if not v.pointer then
        v.pointer = {}
    end
    for i = 1, v.num do
        if i > v.value then
            color_ = 0xcccccc
        end
        if v.data[i] == nil then
            v.data[i] = {}
        end
        v.pointer[i] = cont.create(signal_cont, {
            W = v.data[i].W or ((v.main.W * 2 / 3) / v.num),
            H = v.data[i].H or ((v.main.H) / v.num) * i,
            align = v.data[i].align or lvgl.ALIGN_OUT_RIGHT_BOTTOM,
            align_x = v.data[i].align_x or (v.main.W / 3) / (v.num - 1),
            align_to = v.pointer[i - 1],
            click = false,
            style = {
                border = {
                    width = 0
                },
                bg = {
                    radius = 0,
                    color = color_
                }
            }
        })
    end
    return {
        pointer = v.pointer,
        cont = signal_cont
    }
end

function set_signal(pointer, value)
    for i = 1, #pointer do
        local tempStyle = valid_signal_style
        if i > value then
            tempStyle = invalid_signal_style
        end
        lvgl.obj_add_style(pointer[i], lvgl.CONT_PART_MAIN, tempStyle)
    end
end

v = {
    main = {
        W = 220,
        H = 220,
        -- align = lvgl.ALIGN_IN_TOP_RIGHT,
        -- align_x = -10,
        -- align_y = 10,
        click = false,
        style = {
            border = {
                width = 0
            },
            bg = {
                radius = 10,
                color = 0x4e93fe
            }
        }
    },
    data = {{
        align = lvgl.ALIGN_IN_BOTTOM_LEFT,
        align_x = 2
    }},
    num = 5,
    value = 3,
    pointer = {}
}
-- set_signal(create(lvgl.scr_act(), v).pointer, 1)

-- signal_cont = signal.create(lvgl.scr_act(), {
--     main = {
--         W = 30,
--         H = 30,
--         align = lvgl.ALIGN_IN_TOP_RIGHT,
--         align_x = -10,
--         align_y = 10,
--         click = false,
--         style = {border = {width = 0}, bg = {radius = 0, opa = 0}}
--     },
--     data = {
--         {W = 4, H = 4, align = lvgl.ALIGN_IN_BOTTOM_LEFT, align_x = 2},
--         {H = 11}, {H = 18}, {H = 25}
--     },
--     value = 3,
--     pointer = {}
-- })

-- -- 可使用此接口修改信号值范围1-4
-- --  signal.set_signal(signal_cont.pointer, 2)
