module(..., package.seeall)

local playList = {}
local function cbFnc(result, prompt, head, body)
    log.info("testHttp.cbFnc result", result)
    log.info("testHttp.cbFnc prompt", prompt)
    if result and head then
        for k, v in pairs(head) do
            log.info("testHttp.cbFnc head", k .. ": " .. v)
        end
    end
    if result and body then
        s = json.decode(body)
        playList = s.playlist
        sys.publish("PLAY_LIST")
    end
end
function create(id)
    sys.taskInit(function()
        loadPage.create()
        http.request("GET", URL .. "/user/playlist?uid=" .. id, nil, nil, nil,
                     nil, cbFnc)
        sys.waitUntil("PLAY_LIST")
        lvgl.obj_clean(PARENT_CONT)
        create_s()
    end)

end

function create_s()

    local page_data = {
        W = LCD_W,
        H = LCD_H,
        mode = lvgl.SCRLBAR_MODE_ON,
        style = {
            bg = {
                bg = {color = 0xff0000},
                pad = {all = 0},
                border = {width = 0}
            },
            lable = {bg = {color = 0x00ff00}},
            edge_flash = {bg = {color = 0x0000ff}},
            bar = {bg = {W = 10, H = 10, color = 0}}
        }
    }

    local test_page = page.create(PARENT_CONT, page_data)

    local cont_data = {
        click = false,
        layout = lvgl.LAYOUT_COLUMN_MID,
        align = lvgl.ALIGN_IN_TOP_MID,
        fit = lvgl.FIT_TIGHT,
        style = {border = {width = 0}, bg = {radius = 0}, pad = {all = 0}}
    }
    p_c = cont.create(test_page, cont_data)

    create_music_list()

end

local m_l_c = {
    W = LCD_W * 0.8,
    H = LCD_H / 8,
    click = true,
    style = {border = {width = 1}, bg = {radius = 0}, margin = {all = 0}}
}

local m_l_l = {
    W = LCD_W / 2,
    H = LCD_H / 6,
    click = true,
    -- text = "123456789012345678901234567890",
    mode = lvgl.LABEL_LONG_SROLL,
    style = {
        pad = {all = 0},
        bg = {radius = 0, color = 0xccffcc},
        text = {
            font = style.font24,
            color = 0,
            letter_space = 0,
            line_space = 20
        }
    }
}

function c_m_l_c(v)
    print("歌单名", v.name)
    print("歌单id", v.id)

    print("歌单封面地址", v.coverImgUrl)
    print("歌手数", v.playCount)
    print("歌曲数", v.trackCount)

    print("创建人昵称", v.creator.nickname)
    print("创建人id", v.creator.userId)
    m_l_c.event = function(o, e)
        if e == lvgl.EVENT_CLICKED then
            print(v.name)
            musicList.create(v.id)
        end
    end
    local s_c = cont.create(p_c, m_l_c)
    m_l_l.text = v.name
    m_l_l.event = m_l_c.event
    local s_l = label.create(s_c, m_l_l)

    -- local s_i = img.create(s_c, {
    --     align = lvgl.ALIGN_IN_LEFT_MID,
    --     align_x = LCD_W / 20,
    --     src = "m_l_c.jpg"
    -- })
end
function create_music_list()
    for i = 1, #playList do c_m_l_c(playList[i], i) end
    print("歌单数", #playList)
end

