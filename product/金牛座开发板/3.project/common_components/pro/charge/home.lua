module(..., package.seeall)

require "service_time"
require "service_sim"
require "testUart1"
require "test_send"

p_cont = cont.create(lvgl.scr_act(), {
    W = LCD_W,
    H = LCD_H,
    style = {
        border = {
            width = 0
        },
        bg = {
            radius = 0
        }
    }
})
head_cont = cont.create(p_cont, {
    W = LCD_W,
    H = LCD_H / 8,
    align = lvgl.ALIGN_IN_TOP_MID,
    style = {
        border = {
            width = 0
        },
        bg = {
            radius = 0,
            color = 0x202e45
        }
    }
})

local signal_data = {
    main = {
        W = LCD_H / 9,
        H = LCD_H / 9 - 10,
        align = lvgl.ALIGN_IN_LEFT_MID,
        align_x = 10,
        click = false,
        style = {
            border = {
                width = 0
            },
            bg = {
                radius = 0,
                color = 0x4e93fe,
                opa = 0
            }
        }
    },
    data = {{
        align = lvgl.ALIGN_IN_BOTTOM_LEFT,
        align_x = 2
    }},
    num = 6 -- 信号条有几根，默认五根
    -- value = 3 -- 默认信号值，默认为零
}

local signal_obj = signal.create(head_cont, signal_data)

sys.subscribe("RSSI_CHANGE", function(rssi_value)
    signal.set_signal(signal_obj.pointer, rssi_value + 1)
end)

Unselected_data = {
    img = {
        recolor = 0xffffff
    }
}
selected_data = {
    img = {
        recolor = 0x00ff00
    }
}
local Unselected_style = create_style(Unselected_data)
local selected_style = create_style(selected_data)

local img_data = {
    src = "eme.png",
    align_to = signal_obj.cont,
    -- zoom = 64,
    recolor = true,
    align = lvgl.ALIGN_OUT_RIGHT_MID,
    align_x = LCD_H / 9,
    style = {
        img = {
            recolor = 0xffffff
        }
    }
}

head_obj_list = {}
head_obj_list.eme = img.create(head_cont, img_data)
img_data.src = "gun.png"
img_data.align_to = head_obj_list.eme
head_obj_list.gunA = img.create(head_cont, img_data)
img_data.align_to = head_obj_list.gunA
head_obj_list.gunB = img.create(head_cont, img_data)

local date_label_data = {
    text = "2022-06-22 12:12:12",
    align = lvgl.ALIGN_IN_RIGHT_MID,
    align_x = -LCD_H * 1 / 16,
    style = {
        text = {
            font = style.font24,
            color = 0xffffff,
            letter_space = 0,
            line_space = 0
        }
    }
}
local date_label_obj = label.create(head_cont, date_label_data)

sys.subscribe("SYSTEM_CHANGE_TIME", function(date)
    lvgl.label_set_text(date_label_obj, date)
end)

body_cont = cont.create(p_cont, {
    W = LCD_W,
    H = LCD_H - LCD_H / 8,
    align = lvgl.ALIGN_IN_BOTTOM_MID,
    style = {
        border = {
            width = 0
        },
        bg = {
            radius = 0
        }
    }
})

local list_data = {
    W = LCD_W,
    H = LCD_H - LCD_H / 8,
    icon = {SYMBOL_NEW_LINE, SYMBOL_DIRECTORY, SYMBOL_CLOSE, SYMBOL_EDIT, SYMBOL_USB, SYMBOL_GPS, SYMBOL_STOP,
            SYMBOL_VIDEO},
    btns = {"vol", "electric", "power", "open", "close"},
    event = function(v)
        print("..........", v)
        test_send.send_data({
            id = "A",
            index = v
        })

    end,
    style = {
        bg = {
            bg = {
                color = 0xff0000,
                radius = 0
            },
            border = {
                width = 0
            }
        },
        btn = {
            bg = {
                color = 0x00ffff
            },
            text = {
                color = 0xff0000
            }
        },
        lable = {
            bg = {
                color = 0x0000ff
            }
        },
        edge_flash = {
            bg = {
                color = 0x0000ff
            }
        },
        bar = {
            bg = {
                color = 0xcccccc
            }
        }
    }
}

list.create(body_cont, list_data)

-- -- 二维码容器属性
-- local qrcode_cont_data = {
--     W = (LCD_H * 7 / 16) + 20,
--     H = LCD_H * 9 / 16,
--     align = lvgl.ALIGN_IN_LEFT_MID,
--     align_x = LCD_H * 1 / 16,
--     style = {
--         border = {
--             width = 0
--         },
--         bg = {
--             radius = 10,
--             color = 0xffffff,
--             opa = 255
--         }
--     }
-- }

-- -- 二维码属性
-- local qrcode_data = {
--     W = LCD_H * 7 / 16,
--     H = LCD_H * 7 / 16,
--     align = lvgl.ALIGN_IN_TOP_MID,
--     align_y = 10,
--     text = "hhhhhhhhhhhh"
-- }

-- -- 二维码标签属性
-- local adress_label_data = {
--     W = LCD_H * 7 / 16,
--     align = lvgl.ALIGN_IN_BOTTOM_MID,
--     -- align_y = -10,
--     mode = lvgl.LABEL_LONG_SROLL_CIRC,
--     text = "hhhhhhhhhhhh",
--     style = {
--         text = {
--             font = style.font48,
--             color = 0,
--             opa = 155
--         }
--     }
-- }

-- -- 右侧容器属性
-- local cont_data = {
--     W = LCD_W - (LCD_H * 11 / 16),
--     H = LCD_H - LCD_H / 4,
--     align = lvgl.ALIGN_IN_RIGHT_MID,
--     align_x = -LCD_H * 1 / 16,
--     style = {
--         border = {
--             width = 0
--         },
--         bg = {
--             radius = 10,
--             color = 0x01636c
--         }
--     }
-- }
-- -- tab_view属性
-- local tab_view_data = {
--     W = LCD_W,
--     H = LCD_H - LCD_H / 8,
--     time = 1000,
--     btn_pos = lvgl.TABVIEW_TAB_POS_NONE,
--     page_list = {"", ""},
--     style = {
--         bg = {
--             bg = {
--                 radius = 0,
--                 color = 0x093176
--             }
--         },
--         page = {
--             bg = {
--                 bg = {
--                     color = 0xff0000
--                 },
--                 pad = {
--                     all = 0
--                 }
--             }
--         }

--     }
-- }

-- function create(p, v)
--     local qrcode_cont_obj = cont.create(p, qrcode_cont_data)
--     local qrcode_obj = qrcode.create(qrcode_cont_obj, qrcode_data)
--     local adress_label_obj = label.create(qrcode_cont_obj, adress_label_data)
--     local right_cont = cont.create(p, cont_data)
--     -- local img_data = {
--     --     align = lvgl.ALIGN_IN_TOP_MID,
--     --     align_y = 10,
--     --     src = "empty.png"
--     -- }
--     -- img.create(right_cont, img_data)
--     local label_data = {
--         align = lvgl.ALIGN_IN_TOP_MID,
--         align_y = LCD_H / 16,
--         text = "当前状态",
--         style = {
--             text = {
--                 font = style.font48,
--                 color = 0xffffff,
--                 opa = 155
--             }
--         }
--     }
--     local label_obj = label.create(right_cont, label_data)
--     label_data.style.text.font = style.font24
--     label_data.text = "电压 :  "
--     label_data.align_to = label_obj
--     label_data.align = lvgl.ALIGN_OUT_BOTTOM_LEFT
--     label_data.align_y = LCD_H / 16

--     vol = label.create(right_cont, label_data)

--     label_data.text = "电流 :  "
--     label_data.align_to = vol
--     label_data.align = lvgl.ALIGN_OUT_BOTTOM_MID

--     ele = label.create(right_cont, label_data)

--     label_data.text = "功率 :  "
--     label_data.align_to = ele
--     label_data.align = lvgl.ALIGN_OUT_BOTTOM_MID

--     power = label.create(right_cont, label_data)

--     return {
--         qrcode_obj = qrcode_obj,
--         adress_label_obj = adress_label_obj,
--         qrcode_cont_obj = qrcode_cont_obj
--     }
-- end

-- local tab_view_obj = tab_view.create(body_cont, tab_view_data)

-- local data = {{
--     qrcode_data = "A枪",
--     adress_label_data = "A枪"
-- }, {
--     qrcode_data = "B枪",
--     adress_label_data = "B枪"
-- }}

-- for k, v in pairs(tab_view_obj.page_list) do
--     qrcode_cont_data.text = data[k].qrcode_data
--     adress_label_data.text = data[k].adress_label_data
--     data[k].obj = create(tab_view_obj.page_list[k], data[k])
-- end

-- sys.taskInit(function()
--     local index = 0
--     while true do
--         sys.wait(6000)
--         -- signal.set_signal(signal_obj.pointer, math.random(0, 5))
--         lvgl.tabview_set_tab_act(tab_view_obj.obj, index, lvgl.ANIM_ON)
--         lvgl.obj_add_style(index == 1 and head_obj_list.gunA or head_obj_list.gunB, lvgl.IMG_PART_MAIN, Unselected_style)
--         lvgl.obj_add_style(index == 1 and head_obj_list.gunB or head_obj_list.gunA, lvgl.IMG_PART_MAIN, selected_style)

--         index = (index + 1) % 2
--     end
-- end)

