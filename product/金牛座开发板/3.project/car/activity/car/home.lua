module(..., package.seeall)

-- 更新时间
function update_time()
    local t = os.date("*t")
    local dt = string.format("#FFFFFF %02d-%02d-%02d", t.year, t.month, t.day)

    lvgl.label_set_text(home_date, dt)
    lvgl.obj_align(home_date, nil, lvgl.ALIGN_IN_TOP_MID, -150, 30)

    dt = string.format("#FFFFFF %02d:%02d", t.hour, t.min)

    lvgl.label_set_text(home_time, dt)
    lvgl.obj_align(home_time, nil, lvgl.ALIGN_IN_TOP_MID, 0, 30)

end

_G.contentBodyBG = lvgl.img_create(lvgl.scr_act(), nil)
lvgl.obj_set_click(contentBodyBG, false)
-- lvgl.obj_set_size(contentBodyBG, LCD_W, LCD_H * 7 / 8)
lvgl.img_set_src(contentBodyBG, "/lua/bg_5.jpg")
lvgl.obj_align(contentBodyBG, nil, lvgl.ALIGN_CENTER, 0, 0)

_G.contentBox = cont.create(lvgl.scr_act(), {
    align = lvgl.ALIGN_CENTER,
    W = LCD_W,
    H = LCD_H,
    style = {bg_opa = 0}
})

home_date = label.create(contentBox, {

    recolor = true,
    align = lvgl.ALIGN_IN_TOP_MID,
    align_x = -150,
    align_y = 30,
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font24
})
home_weather = label.create(contentBox, {

    recolor = true,
    align = lvgl.ALIGN_IN_TOP_MID,
    text = "#FFFFFF 晴22摄氏度\n #FFFFFF上海",
    align_x = 150,
    align_y = 30,
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font24
})
home_time = label.create(contentBox, {

    recolor = true,
    align = lvgl.ALIGN_IN_TOP_MID,
    align_y = 30,
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font68
})

-- 更新时间
sys.timerLoopStart(update_time, 1000)

_G.left_arc = arc.create(contentBox, {
    R = 360,
    align = lvgl.ALIGN_IN_LEFT_MID,
    rotation = 110,
    bg_end = 320,
    style = {
        bg = {
            bg_opa = 0,
            bg_color = 0x0000FF,
            line_color = 0xFF0000,
            border_width = 0,
            line_width = 10
        },
        indic = {line_color = 0x4671D5, line_width = 10}
    }
})

_G.right_arc = arc.create(contentBox, {
    R = 360,
    align = lvgl.ALIGN_IN_RIGHT_MID,
    rotation = 110,
    bg_end = 320,
    style = {
        bg = {
            bg_opa = 0,
            bg_color = 0x0000FF,
            line_color = 0x0000FF,
            border_width = 0,
            line_width = 10
        },
        indic = {line_color = 0xFF0000, line_width = 10}
    }
})

left_gauge = gauge.create(left_arc, {
    R = 310,
    align = lvgl.ALIGN_CENTER,
    align_x = -0,
    critical_value = 80,
    style = {
        bg = {bg_opa = 200, border_width = 0},
        major = {font = style.font24}
    }
})
right_gauge = gauge.create(right_arc, {
    R = 310,
    align = lvgl.ALIGN_CENTER,
    align_x = 0,
    critical_value = 80,
    needle_color = 0x000000,
    style = {
        bg = {bg_opa = 200, border_width = 0},
        major = {font = style.font24}
    }
})
function change_rate(rate)
    lvgl.gauge_set_value(left_gauge, 0, rate)
    lvgl.arc_set_value(left_arc, rate)

    lvgl.gauge_set_value(right_gauge, 0, rate)
    lvgl.arc_set_value(right_arc, rate)
end

-- sys.subscribe("RATE", change_rate)

count = 0

-- sys.timerLoopStart(function()
--     count = count + 1
--     count = count % 100
--     sys.publish("RATE", count)
-- end, 10)
index = true

sys.taskInit(function()
    while true do
        change_rate(count)
        if index then
            count = count + 1
        else
            count = count - 1
        end
        if count == 100 or count == 0 then index = not index end

        sys.wait(10)
    end

end)
