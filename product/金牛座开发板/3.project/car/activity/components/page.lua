module(..., package.seeall)

function create_style(v)
    local s = lvgl.style_t()
    lvgl.style_init(s)

    lvgl.style_set_bg_color(s, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.bg_color or 0xFFFFFF))

    lvgl.style_set_border_color(s, bit.band(lvgl.STATE_FOCUSED,
                                            lvgl.STATE_DEFAULT),
                                lvgl.color_hex(v.border_color or 0xFFFFFF))
    -- lvgl.style_set_border_width(s, lvgl.STATE_FOCUSED, 3)

    -- lvgl.style_set_bg_color(s,
    --                         (lvgl.STATE_CHECKED or lvgl.STATE_PRESSED),
    --                         lvgl.color_hex(0xFFFF00))
    -- lvgl.style_set_bg_color(s, lvgl.STATE_DISABLED,
    --                         lvgl.color_hex(0xFFFFFF))
    lvgl.style_set_bg_opa(s, lvgl.STATE_DEFAULT, 100)

end

function create(p, v)
    local tempPage = lvgl.page_create(p, nil)
    lvgl.obj_set_click(tempPage, v.click or true)
    lvgl.obj_set_size(tempPage, v.W or 300, v.H or 200)
    lvgl.obj_align(tempPage, v.align_to, v.align or lvgl.ALIGN_IN_RIGHT_MID,
                   v.align_x or 0, v.align_y or 0)
    lvgl.page_set_scrollbar_mode(tempPage,
                                 v.scrollbar_mode or lvgl.SCROLLBAR_MODE_ON)

    lvgl.obj_add_style(tempPage, lvgl.PAGE_PART_BG, v.style)
    return tempPage
end

