module(..., package.seeall)
--------------
require"ril"
require"ntp"

-- --bTimeSyned ：时间是否已经成功同步过
local bTimeSyned=false

--注册基站时间同步的URC消息处理函数
ril.regUrc("+NITZ", function()    
    log.info("cell.timeSync") 
    bTimeSyned = true
end)

local weekTable={"日","一","二","三","四","五","六"}

function timeService(timeLabel,dateLabel)
    sys.taskInit(function()
        while not bTimeSyned do
            sys.wait(50)
        end
        while _G.HOME and bTimeSyned and timeLabel==_G.TIMELabel and dateLabel==_G.DATELabel do
            local tClock = os.date("*t")
            -- local t=string.format("#FFFFFF %02d:%02d\n#FFFFFF %04d年%02d月%02d日",tClock.hour,tClock.min,tClock.year,tClock.month,tClock.day)
            local time=string.format("#FFFFFF %02d:%02d",tClock.hour,tClock.min)
            lvgl.label_set_text(timeLabel, time)
            local date=string.format("#FFFFFF %02d月%02d日 星期%s",tClock.month,tClock.day,weekTable[tClock.wday])
            lvgl.label_set_text(dateLabel, date)
            sys.wait(1000)
        end
    end)
end
