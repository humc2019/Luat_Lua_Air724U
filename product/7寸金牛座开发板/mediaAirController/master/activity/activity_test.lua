module(..., package.seeall)
--------------

require "misc"
function makeRoot()
    lvgl.obj_clean(contentBox)
    -- statusLabel.setBackImg("/lua/backIcon.png")
    -- lvgl.btn_set_state(backimg,lvgl.STATE_DEFAULT)
    lvgl.obj_set_hidden(backimg,false)
    statusLabel.setWinTitle("测试")

end


function makeButton(CONT)
    local text="1234567890"
    local label_18px = lvgl.label_create(CONT, nil)
    lvgl.label_set_recolor(label_18px, true)  
    local font18 = lvgl.font_load("/lua/datefont.bin")
    lvgl.obj_set_style_local_text_font(label_18px, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
    lvgl.label_set_text(label_18px, "#FFFFFF "..text)
    lvgl.obj_align(label_18px,CONT, lvgl.ALIGN_IN_TOP_LEFT, 50,30)

    local label_40px = lvgl.label_create(CONT, nil)
    lvgl.label_set_recolor(label_40px, true)  
    local font = lvgl.font_load("/lua/timefont.bin")
    lvgl.obj_set_style_local_text_font(label_40px, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font)
    lvgl.label_set_text(label_40px, "#FFFFFF "..text)

    lvgl.obj_align(label_40px,label_18px, lvgl.ALIGN_OUT_BOTTOM_LEFT, 0,50)

    -----------------------
end


function init()
    makeRoot()
    makeButton(contentBox)
    _G.TEST=true
    -- sys.taskInit(testFile)


    
end

-------------------------------------------