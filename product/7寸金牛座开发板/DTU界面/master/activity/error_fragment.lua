module(..., package.seeall)

local topicTable={}
local function topicTableAdd(id,callback)
    topicTable[id]=callback
    sys.subscribe(id,callback)
end

function aa(cont)
-- 创建图片控件
local img = lvgl.img_create(cont, nil)
-- 设置图片显示的图像
lvgl.img_set_src(img, "/lua/t1.png")
-- 图片居中
lvgl.obj_align(img, nil, lvgl.ALIGN_IN_TOP_LEFT, 20, 15)

local label=lvgl.label_create(img, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
    lvgl.label_set_text(label,"#FFFFFF 首页")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_LEFT_MID, 3, 0)

    local label=lvgl.label_create(img, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
    lvgl.label_set_text(label,"#FFFFFF 告警")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -50, 0)

local function event_handler(obj, event)
    if event == lvgl.EVENT_CLICKED then
        BEEP()
        unInit()
        home_fragment.Init()
        print("左按下")
    end
end


    local home=lvgl.cont_create(img, nil)
    lvgl.obj_set_size(home,70, 40)
    lvgl.obj_add_style(home, 0, style.style_div)
    lvgl.obj_set_click(home,true)
    lvgl.obj_set_event_cb(home, event_handler)
end

local function makepage(cont)

    local titlecnt=lvgl.cont_create(cont, nil)
    lvgl.obj_set_size(titlecnt,790, 40)
    lvgl.obj_add_style(titlecnt, 0, style.style_div)
    lvgl.obj_align(titlecnt, nil, lvgl.ALIGN_IN_TOP_MID, 15,73)


    local label=lvgl.label_create(titlecnt, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    lvgl.label_set_text(label,"#909399 序号")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_LEFT, 0, 0)

    local label=lvgl.label_create(titlecnt, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    lvgl.label_set_text(label,"#909399 等级")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_LEFT, 120, 0)


    local label=lvgl.label_create(titlecnt, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    lvgl.label_set_text(label,"#909399 事件内容")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_LEFT, 280, 0)

    local label=lvgl.label_create(titlecnt, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    lvgl.label_set_text(label,"#909399 时间")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_RIGHT, -95, 0)



    ---------------------

    local msgArea=lvgl.page_create(cont, nil)
    lvgl.page_set_scrollbar_mode( msgArea, lvgl.SCRLBAR_MODE_OFF)
    lvgl.obj_set_size(msgArea,790, 330)
    -- lvgl.cont_set_fit(msgArea, lvgl.FIT_TIGHT)
    lvgl.page_set_scrl_layout(msgArea,lvgl.LAYOUT_COLUMN_MID)
    lvgl.obj_align(msgArea, nil, lvgl.ALIGN_IN_BOTTOM_MID, 0,-5)
    -- lvgl.cont_set_layout(msgArea, lvgl.LAYOUT_COLUMN_MID)
    lvgl.obj_add_style(msgArea, lvgl.PAGE_PART_BG, style.style_div)

    local function addmessage(index,level,text,time) 

        local color={
            urgent={
                text="紧急",
                s=style.style_arc_urgent,
            },
            error={
                text="严重",
                s=style.style_arc_error,
            },
            warn={
                text="重要",
                s=style.style_arc_warn,
            },
            nomal={
                text="一般",
                s=style.style_arc_nomal,
            },
            info={
                text="提示",
                s=style.style_arc_info,
            },
}

        local cont=lvgl.cont_create(msgArea, nil)
        lvgl.obj_set_size(cont,750, 30)
        lvgl.obj_add_style(cont, lvgl.CONT_PART_MAIN, style.style_div_blue_lite)

        local label=lvgl.label_create(cont, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label,"#4fb9db "..index)
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_LEFT_MID, 20, 0)

        local dot=lvgl.cont_create(cont, nil)        
        lvgl.obj_add_style(dot, lvgl.CONT_PART_MAIN, color[level].s)
        lvgl.obj_set_size(dot,13, 13)
        lvgl.obj_align(dot, cont, lvgl.ALIGN_IN_LEFT_MID, 113, 0)

        local label=lvgl.label_create(cont, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label,"#4fb9db "..color[level].text)
        lvgl.obj_align(label, dot, lvgl.ALIGN_OUT_RIGHT_MID, 5, 0)

        local temp=label
        local label=lvgl.label_create(cont, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label,"#4fb9db "..text)
        lvgl.obj_align(label, temp, lvgl.ALIGN_OUT_RIGHT_MID, 90, 0)

        local tClock = os.date("*t")
        local date=string.format("%d-%02d-%02d %02d:%02d:%02d",tClock.year,tClock.month,tClock.day,tClock.hour,tClock.min,tClock.sec)

        local label=lvgl.label_create(cont, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label,"#4fb9db "..date)
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -10, 0)
    end

    -- addmessage(1,"urgent","湿度告警,当前值:36.00","2022-03-22 15:39:09") 
    -- addmessage(1,"error","湿度告警,当前值:36.00","2022-03-22 15:39:09") 
    -- addmessage(1,"warn","湿度告警,当前值:36.00","2022-03-22 15:39:09") 
    -- addmessage(1,"nomal","湿度告警,当前值:36.00","2022-03-22 15:39:09") 
    -- addmessage(1,"info","湿度告警,当前值:36.00","2022-03-22 15:39:09") 

    local tt={"urgent","error","warn","nomal","info"}

    for i, v in ipairs(tt) do
        addmessage(i,v,"湿度告警,当前值:36.00") 
    end

    for i, v in ipairs(tt) do
        addmessage(i+5,v,"湿度告警,当前值:36.00","2022-03-22 15:39:09") 
    end
end

function Init()
    lvgl.obj_clean(_G.body)
    aa(_G.body)
    makepage(_G.body)
end

function unInit()
    -- print("取消订阅")
    for id, callback in pairs(topicTable) do
        sys.unsubscribe(id, callback)
    end
end