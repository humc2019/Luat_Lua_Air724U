module(..., package.seeall)

function create_style(v)
    local style_lable = lvgl.style_t()
    lvgl.style_init(style_lable)

    lvgl.style_set_text_font(style_lable, lvgl.STATE_DEFAULT,
                             v.font or style.font24)
    return style_lable
end

function create(p, v)

    local s = lvgl.style_t()

    lvgl.style_init(s)

    -- lvgl.style_set_text_font(s, lvgl.STATE_DEFAULT, font24)

    local l = lvgl.label_create(p, nil)

    -- 设置 Label 的内容
    lvgl.label_set_text(l, v.text or "")

    -- 设置 Label 显示模式
    -- lvgl.LABEL_LONG_BREAK – 保持对象宽度，断开（换行）过长的线条并扩大对象高度
    -- lvgl.LABEL_LONG_DOT – 保持对象大小，打断文本并在最后一行写点（使用 lvgl.label_set_static_text 时不支持）
    -- lvgl.LABEL_LONG_SROLL – 保持大小并来回滚动标签
    -- lvgl.LABEL_LONG_SROLL_CIRC – 保持大小并循环滚动标签  
    -- lvgl.LABEL_LONG_CROP – 保持大小并裁剪文本    
    -- lvgl.LABEL_LONG_EXPAND – 将对象大小扩展为文本大小（默认）
    lvgl.label_set_long_mode(l, v.mode or lvgl.LABEL_LONG_SROLL)

    -- 设置 Label 的显示长度(若设置了显示模式，则需要在其后设置才会有效)
    -- lvgl.obj_set_width(l, v.width or 250)
    lvgl.obj_set_size(l, v.W or 500, v.H or 30)

    -- lvgl.obj_set_style_local_text_font(l, lvgl.LABEL_PART_MAIN,
    --        lvgl.STATE_DEFAULT, style.style_font_24)
    -- 重新着色
    lvgl.label_set_recolor(l, v.recolor or true)

    -- 文本的行可以使用 lvgl.label_set_align(l, lvgl.LABEL_ALIGN_LEFT/RIGHT/CENTER) 
    -- 左右对齐。请注意，它将仅对齐线，而不对齐标签对象本身。
    lvgl.label_set_align(l, v.label_align or lvgl.LABEL_ALIGN_CENTER)

    -- 添加样式
    lvgl.obj_add_style(l, lvgl.LABEL_PART_MAIN, create_style(v))

    -- 设置 Label 的位置
    lvgl.obj_align(l, v.align_to or nil, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    return l
end
