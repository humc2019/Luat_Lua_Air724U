module(..., package.seeall)

local branchs=UIConfig.getBranchTable()
local system=UIConfig.getSystemStatus()

----------------------------------------------------------------------
---状态改变
local function changemode(t,v)
    t.run.ctl=v
    sys.publish(t.path,"CHANGE_MODE")
end

local function changehumi(t,v)
    t.run.humi=v
    sys.publish(t.path,"CHANGE_HUMI")
end

local function changeswitch(t,v)
    t.run.pwr=v
    sys.publish(t.path,"CHANGE_SWITCH")
end

local function changestatus(t,s,v)
    t.run.status=s
    if v then t.run.counterDown=v end
    sys.publish(t.path,"CHANGE_STATUS")
end

function switchDEMO(t)

    

    local i=1
    while true do
        changeswitch(t,i)
        i=i+1
        i=i%2
        sys.wait(500)
    end

end

local function statusDEMO(t)
    sys.wait(2000)
    while true do

        changemode(t,1)
        changestatus(t,1)
        local humi=20
        while humi<=100 do
            changehumi(t,humi)
            humi=humi+1
            sys.wait(50)
        end

        changestatus(t,2)
        while humi>=20 do
            changehumi(t,humi)
            humi=humi-1
            sys.wait(50)
        end

        changemode(t,0)
        local time=80
        while time>=0 do
            changestatus(t,3,time)
            changehumi(t,100-(100*(time/80)))
            time=time-1
            sys.wait(50)
        end

        time=80
        while time>=0 do
            changestatus(t,4,time)
            changehumi(t,100*(time/80))
            time=time-1
            sys.wait(50)
        end
        time=80
        while time>=0 do
            changestatus(t,7,time)
            time=time-1
            sys.wait(50)
        end

        changestatus(t,0,time)
        sys.wait(1000)
        changestatus(t,5,time)
        sys.wait(1000)
        changestatus(t,6,time)
        sys.wait(1000)
        changestatus(t,8,time)
        sys.wait(1000)
        changestatus(t,9,time)
        sys.wait(1000)
        changestatus(t,10,time)
        sys.wait(1000)
        changestatus(t,11,time)
        sys.wait(1000)

    end
end
local function statusDEMO_simple(t)
    while true do
        changestatus(t,0,time)
        sys.wait(1000)
        changestatus(t,1,time)
        sys.wait(1000)
        changestatus(t,2,time)
        sys.wait(1000)
        changestatus(t,3,time)
        sys.wait(1000)
        changestatus(t,4,time)
        sys.wait(1000)
        changestatus(t,5,time)
        sys.wait(1000)
        changestatus(t,6,time)
        sys.wait(1000)
        changestatus(t,7,time)
        sys.wait(1000)
        changestatus(t,8,time)
        sys.wait(1000)
        changestatus(t,9,time)
        sys.wait(1000)
        changestatus(t,10,time)
        sys.wait(1000)
        changestatus(t,11,time)
        sys.wait(1000)

    end
end


local function modeDEMO(t)
    local i=1
    while true do
        changemode(t,i)
        i=i+1
        i=i%2
        sys.wait(1000)
    end
end

local function humiDEMO(t)
    local i=0
    while true do
        changehumi(t,i)
        i=i+1
        i=i%100
        sys.wait(100)
    end
end



-- sys.taskInit(runningTest,"一支路",101)
-- sys.taskInit(runningTest,"二支路",130)
-- sys.taskInit(runningTest,"三支路",191)
-- sys.taskInit(runningTest,"四支路",98)
-- sys.taskInit(runningTest,"五支路",141)
-- sys.taskInit(runningTest,"六支路",122)
sys.taskInit(statusDEMO,branchs[1])
sys.taskInit(modeDEMO,branchs[2])
sys.taskInit(humiDEMO,branchs[2])
sys.taskInit(switchDEMO,branchs[2])
sys.taskInit(statusDEMO_simple,branchs[2])


----------------------------------------------------------------------
----系统状态改变
function systemTest()
    local timer=system.runTimer
    local motor=system.motor
    local pressure=system.pressure
    local tds=system.tds
    local filter=system.filter
    local oil=system.oil
    while true do

        timer=timer+1
        motor=motor+1
        pressure=pressure+1
        tds=tds+1
        filter=filter+1
        oil=oil+1

        motor=motor%3
        pressure=pressure%3
        tds=tds%5
        filter=filter%2
        oil=oil%2

        system.runTimer=timer
        system.motor=motor
        system.pressure=pressure
        system.tds=tds
        system.filter=filter
        system.oil=oil


        sys.publish("SYSTEM_RUNNING")
        sys.publish("SYSTEM_MOTO")
        sys.publish("SYSTEM_PRESSURE")
        sys.publish("SYSTEM_WATER")
        sys.publish("SYSTEM_FILTER")
        sys.publish("SYSTEM_OIL")
        sys.wait(1000)
    end
end

sys.taskInit(systemTest)

----------------------------------------------------------------------
--二维码改变
function changeQR()
    local t={"http://www.opluat.com","http://www.opluat.com","http://www.tencent.com"}
    local i=0
    while true do
        UIConfig.UIParams.qrcode=t[i+1]
        i=i+1
        i=i%3
        sys.publish("CHANGE_QRCODE")
        sys.wait(1000)
    end
end

sys.taskInit(changeQR)


----------------------------------------------------------------------
------时控

function changeTimeCtl(branch,index,i)
    local branchName=branch.path
    local timeSeg=branch.timeControl[index]
    while true do
        timeSeg.ontimeHour=i
        timeSeg.ontimeMinite=i
        timeSeg.offtimeHour=i
        timeSeg.offtimeMinite=i
        timeSeg.switch= (i%2==0 and 1 or 0)
        timeSeg.monday= (i%2==0 and 1 or 0)
        timeSeg.tuesday= (i%2==0 and 0 or 1)
        timeSeg.wednesday= (i%2==0 and 1 or 0)
        timeSeg.thursday= (i%2==0 and 0 or 1)
        timeSeg.friday= (i%2==0 and 1 or 0)
        timeSeg.saturday= (i%2==0 and 0 or 1)
        timeSeg.sunday= (i%2==0 and 1 or 0)
        sys.publish(branchName..timeSeg.seg)
        -- print((branchName..timeSeg.seg))
        i=i+1
        i=i%99
        sys.wait(1000)
    end
end

-- sys.taskInit(changeTimeCtl,branchs[1],1,20)
-- sys.taskInit(changeTimeCtl,branchs[1],2,23)
-- sys.taskInit(changeTimeCtl,branchs[1],3,30)
-- sys.taskInit(changeTimeCtl,branchs[1],4,33)

for _, t in pairs(branchs) do
    for k,v in pairs(t.timeControl) do
        sys.taskInit(changeTimeCtl,t,k,k)
    end
end


----------------------------------------------------------------------参数设置

local function paraSetting(t,i)
    while true do
        t.set.ontime = i
        t.set.offtime = i+10
        t.set.humih = i+10
        t.set.humil = i
        sys.publish(t.path.."_PARACHANGE")
        i=i+1
        i=i%80
        sys.wait(200)
    end
end



for key, value in pairs(branchs) do
    sys.taskInit(paraSetting,value,key)
end
