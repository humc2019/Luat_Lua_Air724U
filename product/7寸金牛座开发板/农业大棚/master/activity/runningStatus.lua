module(..., package.seeall)
-------------------------------------------

RunningSatusTable={
    {
        path="一支路",
        mode="时间",
        humi=64,
        status=135,
        switch=true,
        timeLimit={27,74},
        RHLimit={61,67},
        },
        
        {
        path="二支路",
        mode="湿度",
        humi=50.0,
        status=152,
        switch=true,
        timeLimit={54,85},
        RHLimit={43,61},
        },
        
        {
        path="三支路",
        mode="时间",
        humi=94.0,
        status=180,
        switch=false,
        timeLimit={24,78},
        RHLimit={38,76},
        },
        
        {
        path="四支路",
        mode="湿度",
        humi=70.0,
        status=173,
        switch=false,
        timeLimit={83,84},
        RHLimit={29,100},
        },
        
        {
        path="五支路",
        mode="湿度",
        humi=39.0,
        status=141,
        switch=true,
        timeLimit={75,84},
        RHLimit={62,77},
        },
        
        {
        path="六支路",
        mode="时间",
        humi=62.0,
        status=134,
        switch=true,
        timeLimit={42,70},
        RHLimit={37,59},
        },
        {
        path="七支路",
        mode="时间",
        humi=64.0,
        status=135,
        switch=true,
        timeLimit={27,74},
        RHLimit={61,67},
        },
        
        {
        path="八支路",
        mode="湿度",
        humi=50.0,
        status=152,
        switch=true,
        timeLimit={54,85},
        RHLimit={43,61},
        },
        
        {
        path="九支路",
        mode="时间",
        humi=94.0,
        status=180,
        switch=false,
        timeLimit={24,78},
        RHLimit={38,76},
        },
        
        {
        path="十支路",
        mode="湿度",
        humi=70.0,
        status=173,
        switch=false,
        timeLimit={83,84},
        RHLimit={29,100},
        },
            
}

function getRunningTable()
    return RunningSatusTable
end


systemStatus={
    running={
        text="运行:188H",
        icon="/lua/running_icon.png"
    },
    moto={
        text="电机:正常",
        icon="/lua/moto_icon.png"
    },
    pressure={
        text="压力:正常",
        icon="/lua/pressure_icon.png"
    },
    water={
        text="水质:良好",
        icon="/lua/water_icon.png"
    },
    filter={
        text="滤芯:正常",
        icon="/lua/filter_icon.png"
    },
    oil={
        text="泵油:正常",
        icon="/lua/oil_icon.png"
    }
}